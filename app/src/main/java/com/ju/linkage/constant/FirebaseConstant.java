package com.ju.linkage.constant;

public final class FirebaseConstant {


    public static final String TABLE_USERS = "Users";
    public static final String TABLE_NEWS = "News";
    public static final String TABLE_MISSING = "Missing";
    public static final String TABLE_ADS = "Ads";
    public static final String TABLE_FRIENDS = "Friends";
    public static final String TABLE_REPORT = "reports";
    public static final String TABLE_ROOMS = "rooms";
    public static final String TABLE_SETTING = "Setting";
    public static final String ATTRIBUTE_VERSION_CODE = "versionCode";




    public static final String ADMIN_ACCOUNT = "admin@gmail.com";
    public static final String TABLE_FRIEND_REQUEST = "Friends_Request";

    public static final String STORAGE_UPLOAD_NEWS_PATH = "images/admin/news/";

}
