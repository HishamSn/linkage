package com.ju.linkage.constant;

public class AppConstant {

    public static final int REQUEST_CODE_REGISTER = 2000;
    public static final String SELECTED_LANGUAGE = "Locale.Language";
    public static final String AR = "ar";
    public static final String EN = "en";
}
