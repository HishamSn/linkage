package com.ju.linkage.repoistory;


public interface Function<T> {
    void done(T t);
}
