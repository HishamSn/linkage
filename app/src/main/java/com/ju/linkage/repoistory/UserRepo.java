package com.ju.linkage.repoistory;

import android.support.annotation.NonNull;
import android.widget.Toast;

import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;
import com.ju.linkage.model.User;
import com.ju.linkage.ui.student.friends.FriendsAdapter;

import java.util.ArrayList;
import java.util.List;

import static com.ju.linkage.constant.FirebaseConstant.TABLE_FRIENDS;
import static com.ju.linkage.constant.FirebaseConstant.TABLE_NEWS;
import static com.ju.linkage.constant.FirebaseConstant.TABLE_USERS;

public class UserRepo implements IRepository<User> {


    private static UserRepo instance;
    DatabaseReference databaseReference;
    User userFind;

    UserRepo() {
        databaseReference = FirebaseDatabase.getInstance().getReference().child(TABLE_USERS);
    }

    public static synchronized UserRepo getInstance() {
        if (instance == null) {
            instance = new UserRepo();
        }
        return instance;
    }

    @Override
    public void insert(User item, Function<User> function) {

    }

    public void insertFriend(String id, User user, Function<User> function) {
        List<User> userList = new ArrayList<>();
        databaseReference.child(id).child(TABLE_FRIENDS)
                .addListenerForSingleValueEvent(new ValueEventListener() {
                    @Override
                    public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                        userList.clear();
                        Iterable<DataSnapshot> children = dataSnapshot.getChildren();
                        for (DataSnapshot child : children) {
                            User userFriend = child.getValue(User.class);
                            userList.add(userFriend);
                        }
                        userList.add(user);
                        databaseReference.child(id).child(TABLE_FRIENDS)
                                .setValue(userList).addOnCompleteListener(task -> {

                            function.done(user);

                        });

                    }

                    @Override
                    public void onCancelled(@NonNull DatabaseError databaseError) {
                    }
                });
    }

    @Override
    public void insert(List<User> items, Function<User> function) {

    }

    @Override
    public void update(String value, User item, Function<User> function) {

    }

    @Override
    public void delete(Function<Integer> function) {

    }

    @Override
    public void delete(int id, Function<Integer> function) {

    }

    @Override
    public void delete(String id, Function<String> function) {

    }

    @Override
    public void delete(User item, Function<Integer> function) {

    }

    @Override
    public User findById(String id) {


//        databaseReference.addValueEventListener(new ValueEventListener() {
//            @Override
//            public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
//                Iterable<DataSnapshot> children = dataSnapshot.getChildren();
//                for (DataSnapshot child : children) {
//                    User user = child.getValue(User.class);
//                    if (user.getId().equals(id)) {
//                        userFind = user;
//                    }
//                }
//
//            }
//
//            @Override
//            public void onCancelled(@NonNull DatabaseError databaseError) {
//
//            }
//        });
//
//        return userFind;
        return null;
    }

    public void findById(String id, Function<User> function) {

        databaseReference.addListenerForSingleValueEvent(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                Iterable<DataSnapshot> children = dataSnapshot.getChildren();
                for (DataSnapshot child : children) {
                    User user = child.getValue(User.class);
                    if (user.getId().equals(id)) {
                        function.done(user);
                    }
                }
            }

            @Override
            public void onCancelled(@NonNull DatabaseError databaseError) {

            }
        });
    }


    @Override
    public void findAllSingle(Function<List<User>> function) {

    }

    @Override
    public void findAllRunTime(Function<List<User>> function) {

    }
}
