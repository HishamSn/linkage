package com.ju.linkage.repoistory;

import android.support.annotation.NonNull;

import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;
import com.ju.linkage.model.Missing;


import java.util.ArrayList;
import java.util.List;

import static com.ju.linkage.constant.FirebaseConstant.TABLE_MISSING;

public class MissingRepo implements IRepository<Missing> {

    private static MissingRepo instance;
    DatabaseReference databaseMissing;

    MissingRepo() {
        databaseMissing = FirebaseDatabase.getInstance().getReference().child(TABLE_MISSING);
    }

    public static synchronized MissingRepo getInstance() {
        if (instance == null) {
            instance = new MissingRepo();
        }
        return instance;
    }


    @Override
    public void insert(Missing item, Function<Missing> function) {

    }

    @Override
    public void insert(List<Missing> items, Function<Missing> function) {

    }

    @Override
    public void update(String value, Missing item, Function<Missing> function) {

    }

    @Override
    public void delete(Function<Integer> function) {

    }

    @Override
    public void delete(int id, Function<Integer> function) {
        databaseMissing.child(String.valueOf(id)).removeValue();
        function.done(id);

    }

    @Override
    public void delete(String id, Function<String> function) {

    }

    @Override
    public void delete(Missing item, Function<Integer> function) {

    }

    @Override
    public Missing findById(String id) {
        return null;
    }

    @Override
    public void findAllSingle(Function<List<Missing>> function) {

        List<Missing> missingList = new ArrayList<>();

        databaseMissing.addListenerForSingleValueEvent(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                missingList.clear();
                Iterable<DataSnapshot> children = dataSnapshot.getChildren();
                for (DataSnapshot child : children) {
                    Missing missing = child.getValue(Missing.class);
                    missingList.add(missing);
                }
                function.done(missingList);
            }

            @Override
            public void onCancelled(@NonNull DatabaseError databaseError) {
            }
        });
    }

    @Override
    public void findAllRunTime(Function<List<Missing>> function) {

        List<Missing> missingList = new ArrayList<>();

        databaseMissing.addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                missingList.clear();
                Iterable<DataSnapshot> children = dataSnapshot.getChildren();
                for (DataSnapshot child : children) {
                    Missing missing = child.getValue(Missing.class);
                    missingList.add(missing);
                }
                function.done(missingList);
            }

            @Override
            public void onCancelled(@NonNull DatabaseError databaseError) {
            }
        });
    }
}
