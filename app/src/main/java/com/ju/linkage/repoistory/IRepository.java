package com.ju.linkage.repoistory;

import java.util.List;


public interface IRepository<T> {

    void insert(T item, Function<T> function);

    void insert(List<T> items, Function<T> function);

    void update(String value,T item, Function<T> function);

    void delete(Function<Integer> function);

    void delete(int id, Function<Integer> function);

    void delete(String id, Function<String> function);

    void delete(T item, Function<Integer> function);

    T findById(String id);

    void findAllSingle(Function<List<T>> function);

    void findAllRunTime(Function<List<T>> function);

}
