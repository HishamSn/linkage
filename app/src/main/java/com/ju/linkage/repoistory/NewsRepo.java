package com.ju.linkage.repoistory;

import android.support.annotation.NonNull;

import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;
import com.ju.linkage.model.News;

import java.util.ArrayList;
import java.util.List;

import static com.ju.linkage.constant.FirebaseConstant.TABLE_NEWS;

public class NewsRepo implements IRepository<News> {

    private static NewsRepo instance;
    DatabaseReference databaseNews;

    NewsRepo() {
        databaseNews = FirebaseDatabase.getInstance().getReference().child(TABLE_NEWS);
    }

    public static synchronized NewsRepo getInstance() {
        if (instance == null) {
            instance = new NewsRepo();
        }
        return instance;
    }


    @Override
    public void insert(News item, Function<News> function) {

    }

    @Override
    public void insert(List<News> items, Function<News> function) {

    }

    @Override
    public void update(String value, News item, Function<News> function) {

    }

    @Override
    public void delete(Function<Integer> function) {

    }

    @Override
    public void delete(int id, Function<Integer> function) {
        databaseNews.child(String.valueOf(id)).removeValue();
        function.done(id);

    }

    @Override
    public void delete(String id, Function<String> function) {

    }

    @Override
    public void delete(News item, Function<Integer> function) {

    }

    @Override
    public News findById(String id) {
        return null;
    }

    @Override
    public void findAllSingle(Function<List<News>> function) {

        List<News> newsList = new ArrayList<>();

        databaseNews.addListenerForSingleValueEvent(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                newsList.clear();
                Iterable<DataSnapshot> children = dataSnapshot.getChildren();
                for (DataSnapshot child : children) {
                    News news = child.getValue(News.class);
                    newsList.add(news);
                }
                function.done(newsList);
            }

            @Override
            public void onCancelled(@NonNull DatabaseError databaseError) {
            }
        });
    }

    @Override
    public void findAllRunTime(Function<List<News>> function) {

        List<News> newsList = new ArrayList<>();

        databaseNews.addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                newsList.clear();
                Iterable<DataSnapshot> children = dataSnapshot.getChildren();
                for (DataSnapshot child : children) {
                    News news = child.getValue(News.class);
                    newsList.add(news);
                }
                function.done(newsList);
            }

            @Override
            public void onCancelled(@NonNull DatabaseError databaseError) {
            }
        });
    }
}
