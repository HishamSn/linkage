package com.ju.linkage.repoistory;

import android.support.annotation.NonNull;

import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;

import static com.ju.linkage.constant.FirebaseConstant.ATTRIBUTE_VERSION_CODE;
import static com.ju.linkage.constant.FirebaseConstant.TABLE_SETTING;

public class SettingRepo  {


    private static SettingRepo instance;
    DatabaseReference databaseReference;

    SettingRepo() {
        databaseReference = FirebaseDatabase.getInstance().getReference().child(TABLE_SETTING);
    }

    public static synchronized SettingRepo getInstance() {
        if (instance == null) {
            instance = new SettingRepo();
        }
        return instance;
    }


    public void getVersionEventListener(Function<Integer> function) {

        databaseReference.child(ATTRIBUTE_VERSION_CODE).addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                Integer versionCode = dataSnapshot.getValue(Integer.class);

                function.done(versionCode);


            }

            @Override
            public void onCancelled(@NonNull DatabaseError databaseError) {

            }
        });

    }
}
