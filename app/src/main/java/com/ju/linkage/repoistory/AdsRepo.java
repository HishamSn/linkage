package com.ju.linkage.repoistory;

import android.support.annotation.NonNull;

import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;
import com.ju.linkage.model.Ads;

import java.util.ArrayList;
import java.util.List;

import static com.ju.linkage.constant.FirebaseConstant.TABLE_ADS;

public class AdsRepo implements IRepository<Ads> {

    private static AdsRepo instance;
    DatabaseReference databaseAds;

    AdsRepo() {
        databaseAds = FirebaseDatabase.getInstance().getReference().child(TABLE_ADS);
    }

    public static synchronized AdsRepo getInstance() {
        if (instance == null) {
            instance = new AdsRepo();
        }
        return instance;
    }


    @Override
    public void insert(Ads item, Function<Ads> function) {

    }

    @Override
    public void insert(List<Ads> items, Function<Ads> function) {

    }

    @Override
    public void update(String value, Ads item, Function<Ads> function) {

    }

    @Override
    public void delete(Function<Integer> function) {

    }

    @Override
    public void delete(int id, Function<Integer> function) {
        databaseAds.child(String.valueOf(id)).removeValue();
        function.done(id);

    }

    @Override
    public void delete(String id, Function<String> function) {

    }

    @Override
    public void delete(Ads item, Function<Integer> function) {

    }

    @Override
    public Ads findById(String id) {
        return null;
    }

    @Override
    public void findAllSingle(Function<List<Ads>> function) {

        List<Ads> adsList = new ArrayList<>();

        databaseAds.addListenerForSingleValueEvent(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                adsList.clear();
                Iterable<DataSnapshot> children = dataSnapshot.getChildren();
                for (DataSnapshot child : children) {
                    Ads ads = child.getValue(Ads.class);
                    adsList.add(ads);
                }
                function.done(adsList);
            }

            @Override
            public void onCancelled(@NonNull DatabaseError databaseError) {
            }
        });
    }

    @Override
    public void findAllRunTime(Function<List<Ads>> function) {

        List<Ads> adsList = new ArrayList<>();

        databaseAds.addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                adsList.clear();
                Iterable<DataSnapshot> children = dataSnapshot.getChildren();
                for (DataSnapshot child : children) {
                    Ads ads = child.getValue(Ads.class);
                    adsList.add(ads);
                }
                function.done(adsList);
            }

            @Override
            public void onCancelled(@NonNull DatabaseError databaseError) {
            }
        });
    }
}
