package com.ju.linkage.repoistory;

import android.support.annotation.NonNull;

import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.ju.linkage.model.FriendRequest;

import java.util.List;

import static com.ju.linkage.constant.FirebaseConstant.TABLE_FRIEND_REQUEST;

public class FriendRequestRepo implements IRepository<FriendRequest> {

    private static FriendRequestRepo instance;
    DatabaseReference databaseReference;

    FriendRequestRepo() {
        databaseReference = FirebaseDatabase.getInstance().getReference().child(TABLE_FRIEND_REQUEST);
    }

    public static synchronized FriendRequestRepo getInstance() {
        if (instance == null) {
            instance = new FriendRequestRepo();
        }
        return instance;
    }


    @Override
    public void insert(FriendRequest item, Function<FriendRequest> function) {

    }

    @Override
    public void insert(List<FriendRequest> items, Function<FriendRequest> function) {

    }

    @Override
    public void update(String value, FriendRequest item, Function<FriendRequest> function) {

    }

    @Override
    public void delete(Function<Integer> function) {

    }

    @Override
    public void delete(int id, Function<Integer> function) {

    }

    @Override
    public void delete(String id, Function<String> function) {
        databaseReference.child(id).removeValue().addOnCompleteListener(new OnCompleteListener<Void>() {
            @Override
            public void onComplete(@NonNull Task<Void> task) {
                function.done(id);
            }

        }).addOnFailureListener(new OnFailureListener() {
            @Override
            public void onFailure(@NonNull Exception e) {
                function.done(id);
            }
        });
    }

    @Override
    public void delete(FriendRequest item, Function<Integer> function) {

    }



    @Override
    public FriendRequest findById(String id) {
        return null;
    }

    @Override
    public void findAllSingle(Function<List<FriendRequest>> function) {

    }

    @Override
    public void findAllRunTime(Function<List<FriendRequest>> function) {

    }
}
