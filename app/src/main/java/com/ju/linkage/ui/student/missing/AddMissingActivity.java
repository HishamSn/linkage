package com.ju.linkage.ui.student.missing;

import android.app.ProgressDialog;
import android.content.Intent;
import android.graphics.Bitmap;
import android.net.Uri;
import android.os.Bundle;
import android.provider.MediaStore;
import android.support.annotation.Nullable;
import android.support.v7.widget.AppCompatButton;
import android.support.v7.widget.AppCompatEditText;
import android.support.v7.widget.AppCompatImageView;
import android.view.View;
import android.view.WindowManager;
import android.widget.Toast;

import com.bumptech.glide.Glide;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.storage.FirebaseStorage;
import com.google.firebase.storage.StorageReference;
import com.google.firebase.storage.UploadTask;
import com.ju.linkage.R;
import com.ju.linkage.model.Missing;
import com.ju.linkage.model.User;
import com.ju.linkage.repoistory.MissingRepo;
import com.ju.linkage.repoistory.UserRepo;
import com.ju.linkage.ui.base.BaseActivity;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

import static com.ju.linkage.constant.FirebaseConstant.TABLE_MISSING;

public class AddMissingActivity extends BaseActivity {

    private static final int PICK_IMAGE_REQUEST = 71;
    private Uri filePath;
    private FirebaseStorage storage;
    private String missingImageUrl;
    private DatabaseReference databaseMissing;
    private List<Missing> missingList;
    StorageReference mStorageReference;

    @BindView(R.id.iv_missing)
    AppCompatImageView ivMissing;
    @BindView(R.id.et_details)
    AppCompatEditText etDetails;
    @BindView(R.id.btn_add)
    AppCompatButton btnSave;
    @BindView(R.id.et_phone)
    AppCompatEditText etPhone;

    private User user;
    private FirebaseAuth mAuth;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_add_missing);
        getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_ADJUST_PAN);
        ButterKnife.bind(this);

        init();
        getUserInfo();

    }

    private void getUserInfo() {
        UserRepo.getInstance().findById(mAuth.getUid(), student -> {
            user = student;
        });
    }
    private void init() {
        mAuth = FirebaseAuth.getInstance();
        missingList = new ArrayList<>();
        mStorageReference = FirebaseStorage.getInstance().getReference();
    }


    @OnClick({R.id.iv_missing, R.id.btn_add})
    public void onViewClicked(View view) {
        switch (view.getId()) {
            case R.id.iv_missing:
                chooseImage();

                break;
            case R.id.btn_add:

                if (filePath != null) {
                    uploadMissingToFirebase();
                } else {
                    Toast.makeText(this,"Please Insert The photo",Toast.LENGTH_SHORT).show();
                }


                break;
        }
    }

    private void chooseImage() {
        Intent intent = new Intent();
        intent.setType("image/*");
        intent.setAction(Intent.ACTION_GET_CONTENT);
        startActivityForResult(Intent.createChooser(intent, "Select Picture"), PICK_IMAGE_REQUEST);
    }


    private void uploadMissingToFirebase() {
        databaseMissing = FirebaseDatabase.getInstance().getReference().child(TABLE_MISSING);


        MissingRepo.getInstance().findAllSingle(missingList -> {
            this.missingList.clear();
            this.missingList.addAll(missingList);
            addToDatabase();
        });

    }

    private void upLoadPhotoToFirebase(int id) {
        if (filePath != null) {
            final ProgressDialog progressDialog = new ProgressDialog(this);
            progressDialog.setTitle(getString(R.string.Uploading));
            progressDialog.show();

            StorageReference storageRef = mStorageReference.child("missing/" + id);

            UploadTask uploadTask = storageRef.putFile(filePath);
            uploadTask.continueWithTask(task -> {
                if (!task.isSuccessful()) {
                    progressDialog.dismiss();
                    Toast.makeText(AddMissingActivity.this, "Failed " + task.getException().getMessage(), Toast.LENGTH_SHORT).show();
                    throw task.getException();
                }
                return storageRef.getDownloadUrl();
            }).addOnCompleteListener(task -> {
                if (task.isSuccessful()) {
                    Uri downloadUri = task.getResult();
                    missingImageUrl = String.valueOf(downloadUri);

                    missingList.get(id).setImage(missingImageUrl);

                    databaseMissing.child(String.valueOf(id)).setValue(missingList.get(id));

                    progressDialog.dismiss();
                    Toast.makeText(AddMissingActivity.this, "Uploaded", Toast.LENGTH_SHORT).show();
                    finish();

                } else {
                    progressDialog.dismiss();
                    Toast.makeText(AddMissingActivity.this, "Failed " + task.getException().getMessage(), Toast.LENGTH_SHORT).show();
                }
            });
        }
    }

    private void addToDatabase() {
        Missing missing = new Missing();
        missing.setId(missingList.size() + "");
        missing.setText(etDetails.getText().toString());
        missing.setPhone(etPhone.getText().toString());
        missing.setUser(user);

        missingList.add(missing);
        databaseMissing.setValue(missingList);

        upLoadPhotoToFirebase(missingList.size() - 1);

    }


    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        if (requestCode == PICK_IMAGE_REQUEST && resultCode == RESULT_OK &&
                data != null && data.getData() != null) {
            filePath = data.getData();
            try {

                Bitmap bitmap = MediaStore.Images.Media.getBitmap(getContentResolver(), filePath);
                Glide.with(AddMissingActivity.this)
                        .load(bitmap)
                        .into(ivMissing);


            } catch (Exception e) {
                Toast.makeText(AddMissingActivity.this, "Failed onActivityResult" + e.getMessage(), Toast.LENGTH_SHORT).show();

            }
        }
    }
}
