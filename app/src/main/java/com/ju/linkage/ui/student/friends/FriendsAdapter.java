package com.ju.linkage.ui.student.friends;

import android.content.Context;
import android.content.Intent;
import android.support.annotation.NonNull;
import android.support.v7.widget.AppCompatTextView;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.bumptech.glide.Glide;
import com.ju.linkage.R;
import com.ju.linkage.model.User;
import com.ju.linkage.ui.student.chat.FriendsChatActivity;
import com.ju.linkage.ui.student.chat.GroupChatActivity;

import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import de.hdodenhof.circleimageview.CircleImageView;


public class FriendsAdapter extends RecyclerView.Adapter<FriendsAdapter.ViewHolder> {


    private static final int ROW_FRIENDS = R.layout.row_friend_name;
    private static final int ROW_GROUP = R.layout.row_group;

    private Context context;
    private List<User> friendsList;
    private boolean hasProgress = true;

    public FriendsAdapter(List<User> friendsList) {
        this.friendsList = friendsList;
    }

    public FriendsAdapter() {
    }


    @Override
    public int getItemViewType(int position) {
        if (position == 0) {
            return ROW_GROUP;
        }
        return ROW_FRIENDS;
    }


    @Override
    public void onAttachedToRecyclerView(RecyclerView recyclerView) {
        super.onAttachedToRecyclerView(recyclerView);
        context = recyclerView.getContext();
    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(viewType,
                parent, false);
        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolder holder, int position) {
        if (position == 0) {
            holder.itemView.setOnClickListener(v -> {
                Intent intent = new Intent(context, GroupChatActivity.class);
                intent.putExtra("name", "IT Factory Chat");

                context.startActivity(intent);
            });


            return;
        }
        holder.tvName.setText(friendsList.get(position).getFullName());


        holder.itemView.setOnClickListener(v -> {
            Intent intent = new Intent(context, FriendsChatActivity.class);

            intent.putExtra("friend",friendsList.get(position));


            context.startActivity(intent);
        });

        holder.itemView.setOnLongClickListener(new View.OnLongClickListener() {
            @Override
            public boolean onLongClick(View v) {
                return false;
            }
        });
    }

    private void loadImage(ViewHolder holder, String url) throws Exception {
        Glide.with(holder.ivUser).load(url).into(holder.ivUser);

//        int[] arr = {0, 1, 2};
//        for (int i : arr) {
//            String imageName = "image" + i;
//            int resId = context.getResources().getIdentifier(imageName, "drawable", context.getPackageName());
//            context.getResources().getDrawable(resId);
//        }
    }


    @Override
    public int getItemCount() {
        // if we don't need loading in the first
//        return friendsList.size() > 0 ?
//                hasProgress ? friendsList.size() + 1 : friendsList.size() : 0;

//        return hasProgress ? friendsList.size() + 1 : friendsList.size();
        return friendsList.size();
    }


    public void disableProgress() {
        hasProgress = false;
    }

    public void activeProgress() {
        hasProgress = true;
    }

    public class ViewHolder extends RecyclerView.ViewHolder {

        @BindView(R.id.iv_user)
        CircleImageView ivUser;
        @BindView(R.id.tv_name)
        AppCompatTextView tvName;

        public ViewHolder(View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);

        }
    }
}
