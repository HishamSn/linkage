package com.ju.linkage.ui.admin.dashboard;

import android.content.Intent;
import android.graphics.Color;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuItem;

import com.ju.linkage.R;
import com.ju.linkage.model.ModelRowImage;
import com.ju.linkage.ui.auth.LoginActivity;
import com.ju.linkage.ui.base.BaseActivity;
import com.ju.linkage.utils.SessionUtils;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;

public class DashboardActivity extends BaseActivity {
    @BindView(R.id.rv_dashboard)
    RecyclerView rvDashboard;
    @BindView(R.id.toolbar)
    Toolbar toolbar;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_dashboard);
        ButterKnife.bind(this);

        setSupportActionBar(toolbar);

        getSupportActionBar().setDisplayShowTitleEnabled(false);

       toolbar.setTitle("Admin Dashboard");
       toolbar.setTitleTextColor(Color.WHITE);

        setUpRecyclerView(getMainList());

    }


    private void setUpRecyclerView(List<ModelRowImage> mainList) {
        rvDashboard.setLayoutManager(
                new LinearLayoutManager(this));

        rvDashboard.setAdapter(new DashboardAdapter(mainList));
    }

    @NonNull
    private List<ModelRowImage> getMainList() {
        List<ModelRowImage> dashboardList = new ArrayList<>();
        dashboardList.add(new ModelRowImage("News", R.drawable.bg_news));
        dashboardList.add(new ModelRowImage("Ads", R.drawable.bg_ads));
        dashboardList.add(new ModelRowImage("Reports", R.drawable.bg_report_box));

        return dashboardList;
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu_main, menu);

        return super.onCreateOptionsMenu(menu);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {

        if (item.getItemId() == R.id.action_logout) {
            SessionUtils.getInstance().logout();
            startActivity(new Intent(this, LoginActivity.class));
            finish();
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

}
