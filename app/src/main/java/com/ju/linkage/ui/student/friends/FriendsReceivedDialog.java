package com.ju.linkage.ui.student.friends;

import android.app.Dialog;
import android.content.Context;
import android.support.v7.widget.AppCompatTextView;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.ViewGroup;

import com.ju.linkage.R;
import com.ju.linkage.model.User;

import java.util.List;

import butterknife.BindView;

public class FriendsReceivedDialog extends Dialog {
    @BindView(R.id.tv_label_friend_received)
    AppCompatTextView tvLabelFriendReceived;
    @BindView(R.id.rv_friends_received)
    RecyclerView rvFriendsReceived;

    public FriendsReceivedDialog(Context context) {
        super(context);
        setContentView(R.layout.dialog_friends_received);
        getWindow()
                .setLayout(
                        ViewGroup.LayoutParams.MATCH_PARENT,
                        ViewGroup.LayoutParams.MATCH_PARENT
                );
        rvFriendsReceived = findViewById(R.id.rv_friends_received);
        rvFriendsReceived.setLayoutManager(new LinearLayoutManager(context));

    }


}
