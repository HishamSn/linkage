package com.ju.linkage.ui.student.main;

import android.content.Context;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;

import com.ju.linkage.ui.student.ads.AdsFragment;
import com.ju.linkage.ui.student.news.NewsFragment;
import com.ju.linkage.ui.student.friends.FriendsFragment;

public class MainViewPagerAdapter extends FragmentPagerAdapter {
    final int PAGE_COUNT = 3;
    private String tabTitles[] = new String[]{"Friends", "News", "Ads"};
    private Context context;

    public MainViewPagerAdapter(FragmentManager fm, Context context) {
        super(fm);
        this.context = context;
    }

    @Override
    public Fragment getItem(int position) {

        switch (position) {

            case 0:
                return new FriendsFragment();

            case 1:
                return new NewsFragment();

            case 2:
                return new AdsFragment();

            default:
                return new FriendsFragment();

        }

    }

    @Override
    public int getCount() {
        return PAGE_COUNT;
    }

    @Override
    public CharSequence getPageTitle(int position) {
        // Generate title based on item position
        return tabTitles[position];
    }
}
