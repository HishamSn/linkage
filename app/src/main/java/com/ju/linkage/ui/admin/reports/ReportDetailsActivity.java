package com.ju.linkage.ui.admin.reports;

import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.AppCompatTextView;

import com.ju.linkage.R;
import com.ju.linkage.utils.Utils;
import com.yarolegovich.lovelydialog.LovelyProgressDialog;

import butterknife.BindView;
import butterknife.ButterKnife;

public class ReportDetailsActivity extends AppCompatActivity {

    @BindView(R.id.tv_name)
    AppCompatTextView tvName;
    @BindView(R.id.tv_details_report)
    AppCompatTextView tvDetailsReport;
    @BindView(R.id.tv_title)
    AppCompatTextView tvTitle;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_report_details);
        ButterKnife.bind(this);

        tvName.setText(getIntent().getExtras().getString("name") + " Report");
        tvTitle.setText(getIntent().getExtras().getString("title"));
        tvDetailsReport.setText(getIntent().getExtras().getString("description"));



    }
}
