package com.ju.linkage.ui.student.main;

import android.content.Intent;
import android.os.Bundle;
import android.support.design.widget.NavigationView;
import android.support.design.widget.TabLayout;
import android.support.v4.app.NotificationManagerCompat;
import android.support.v4.view.GravityCompat;
import android.support.v4.view.ViewPager;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.TextView;

import com.ju.linkage.R;
import com.ju.linkage.prefs.PrefsUtils;
import com.ju.linkage.ui.about.AboutActivity;
import com.ju.linkage.ui.auth.LoginActivity;
import com.ju.linkage.ui.base.BaseActivity;
import com.ju.linkage.ui.student.links.LinksActivity;
import com.ju.linkage.ui.student.missing.MissingActivity;
import com.ju.linkage.ui.student.report.AddReportActivity;
import com.ju.linkage.utils.SessionUtils;

import butterknife.BindView;
import butterknife.ButterKnife;

public class MainActivity extends BaseActivity {


    @BindView(R.id.tabs)
    TabLayout tabs;
    @BindView(R.id.viewpager)
    ViewPager viewPager;
    @BindView(R.id.toolbar)
    Toolbar toolbar;
    @BindView(R.id.nav_main)
    NavigationView navMain;
    @BindView(R.id.drawer_layout)
    DrawerLayout drawerLayout;

    View viewHeaderNav;

    private ActionBarDrawerToggle toggleBar;
    private NotificationManagerCompat notificationManager;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        ButterKnife.bind(this);

        notificationManager = NotificationManagerCompat.from(this);

        viewPager.setAdapter(new MainViewPagerAdapter(getSupportFragmentManager(),
                MainActivity.this));
        tabs.setupWithViewPager(viewPager);

        toggleBar = new ActionBarDrawerToggle(this, drawerLayout, toolbar
                , R.string.navigation_drawer_open, R.string.navigation_drawer_close);
        drawerLayout.addDrawerListener(toggleBar);
        toggleBar.syncState();
        navMain.getMenu().getItem(0).setChecked(true);
        setNavigationItemSelectedListener(navMain);


        setSupportActionBar(toolbar);
        toolbar.setTitle("");

        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setHomeButtonEnabled(true);
        getSupportActionBar().setDisplayShowTitleEnabled(false);
        addHeader();
        setUpViewHeader();


    }


    public void setUpViewHeader() {
        viewHeaderNav.findViewById(R.id.btn_logout).setOnClickListener(v -> {
            startActivity(new Intent(this, LoginActivity.class));
            SessionUtils.getInstance().logout();
            finish();
        });

        ((TextView) viewHeaderNav.findViewById(R.id.tv_name))
                .setText(PrefsUtils.getInstance().getStudentName());

    }


    private void setNavigationItemSelectedListener(NavigationView navigationView) {
        navigationView.setNavigationItemSelectedListener(item -> {
            selectItemDrawer(item);
            return true;
        });
    }

    private void selectItemDrawer(MenuItem menuItem) {
        switch (menuItem.getItemId()) {
            case R.id.menu_links:
                startActivity(new Intent(this, LinksActivity.class));
                break;

            case R.id.menu_lost_items:
                startActivity(new Intent(this, MissingActivity.class));
                break;

            case R.id.menu_about:
                startActivity(new Intent(this, AboutActivity.class));
                break;

            case R.id.menu_rate_us:
                startActivity(new Intent(this, AddReportActivity.class));
                break;

        }
        drawerLayout.closeDrawers();


    }

    private void addHeader() {
        viewHeaderNav = navMain.inflateHeaderView(R.layout.nav_header);
    }

    @Override
    public void onBackPressed() {
        if (drawerLayout != null && drawerLayout.isDrawerOpen(GravityCompat.START)) {
            drawerLayout.closeDrawer(GravityCompat.START);
            return;
        }

        super.onBackPressed();
    }


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu_main, menu);

        return super.onCreateOptionsMenu(menu);
    }


    @Override
    public boolean onOptionsItemSelected(MenuItem item) {

//        if (item.getItemId() == R.id.action_notfication) {
//            askForPermission(Manifest.permission.ACCESS_COARSE_LOCATION, LOCATION);
//            return true;
//        }

        if (toggleBar.onOptionsItemSelected(item)) {
            return true;
        }
        return super.onOptionsItemSelected(item);
    }


}
