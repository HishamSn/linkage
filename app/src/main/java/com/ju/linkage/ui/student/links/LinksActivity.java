package com.ju.linkage.ui.student.links;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.widget.RecyclerView;

import com.ju.linkage.R;
import com.ju.linkage.model.Links;
import com.ju.linkage.ui.base.BaseActivity;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;


public class LinksActivity extends BaseActivity {
    @BindView(R.id.rv_links)
    RecyclerView rvLinks;

    private List<Links> linksList;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_links);
        ButterKnife.bind(this);

        init();

        fillList();
        rvLinks.setAdapter(new LinksAdapter(linksList));
    }

    private void fillList() {
        linksList.add(new Links("الكليات الانسانية", null));
        linksList.add(new Links("كلية الآداب", "http://arts.ju.edu.jo/ar/arabic/Home.aspx"));
        linksList.add(new Links("كلية الأعمال", "http://business.ju.edu.jo/ar/arabic/Home.aspx"));
        linksList.add(new Links("كلية الشريعة", "http://sharia.ju.edu.jo/ar/arabic/Home.aspx"));
        linksList.add(new Links("كلية العلوم التربوية", "http://educational.ju.edu.jo/ar/arabic/Home.aspx"));
        linksList.add(new Links("كلية الحقوق", "http://law.ju.edu.jo/ar/arabic/Home.aspx"));
        linksList.add(new Links("كلية التربية الرياضة", "http://physicalstudies.ju.edu.jo/ar/arabic/Home.aspx"));
        linksList.add(new Links("كلية الفنون والتصميم", "http://artsdesign.ju.edu.jo/ar/arabic/Home.aspx"));
        linksList.add(new Links("كلية الأمير الحسين بن عبدالله الثاني للدراسات الدولية", "http://international.ju.edu.jo/ar/arabic/Home.aspx"));
        linksList.add(new Links("كلية اللغات الاجنبية", "http://languages.ju.edu.jo/ar/arabic/Home.aspx"));
        linksList.add(new Links("كلية الآثار والسياحة", "http://archaeology.ju.edu.jo/ar/arabic/Home.aspx"));
        linksList.add(new Links("الكليات العلمية", null));
        linksList.add(new Links("كلية العلوم", "http://science.ju.edu.jo/ar/arabic/Home.aspx"));
        linksList.add(new Links("كلية الزراعة", "http://agriculture.ju.edu.jo/ar/arabic/Home.aspx"));
        linksList.add(new Links("كلية الهندسة", "http://engineering.ju.edu.jo/ar/arabic/Home.aspx"));
        linksList.add(new Links("كلية الملك عبدالله الثاني لتكنولوجيا المعلومات", "http://computer.ju.edu.jo/ar/arabic/Home.aspx"));
        linksList.add(new Links("الكليات الصحية", null));
        linksList.add(new Links("كلية الطب", "http://medicine.ju.edu.jo/ar/arabic/Home.aspx"));
        linksList.add(new Links("كلية التمريض", "http://nursing.ju.edu.jo/ar/arabic/Home.aspx"));
        linksList.add(new Links("كلية الصيدلة", "http://pharmacy.ju.edu.jo/ar/arabic/Home.aspx"));
        linksList.add(new Links("كلية طب الاسنان", "http://dentistry.ju.edu.jo/ar/arabic/Home.aspx"));
        linksList.add(new Links("كلية علوم التأهيل", "http://rehabilitation.ju.edu.jo/ar/arabic/Home.aspx"));
        linksList.add(new Links("كليات فرع العقبة", null));
        linksList.add(new Links("كلية نظم وتكنولوجيا المعلومات", "http://it.ju.edu.jo/ar/arabic/Home.aspx"));
        linksList.add(new Links("كلية اللغات", "http://aqlanguages.ju.edu.jo/ar/arabic/Home.aspx"));
        linksList.add(new Links("كلية السياحة والفندقة", "http://tourism.ju.edu.jo/ar/arabic/Home.aspx"));
        linksList.add(new Links("كلية الادارة والتمويل", "http://management.ju.edu.jo/ar/arabic/Home.aspx"));
        linksList.add(new Links("كلية العلوم البحرية", "http://marine.ju.edu.jo/ar/arabic/Home.aspx"));
        linksList.add(new Links("العمادات", null));
        linksList.add(new Links("عمادة البحث العلمي", "http://research.ju.edu.jo/ar/arabic/Home.aspx"));
        linksList.add(new Links("عمادة شؤون الطلبة", "http://studentaffairs.ju.edu.jo/home.aspx"));
        linksList.add(new Links("الدراسات العليا", null));
        linksList.add(new Links("كلية الدراسات العليا", "http://graduatedstudies.ju.edu.jo/ar/arabic/Home.aspx"));

    }

    private void init() {
        linksList = new ArrayList<>();
    }


}
