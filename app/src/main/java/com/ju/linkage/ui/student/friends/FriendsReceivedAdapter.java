package com.ju.linkage.ui.student.friends;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.v7.widget.AppCompatButton;
import android.support.v7.widget.AppCompatTextView;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Toast;

import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.ju.linkage.R;
import com.ju.linkage.model.User;
import com.ju.linkage.repoistory.FriendRequestRepo;
import com.ju.linkage.repoistory.UserRepo;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import de.hdodenhof.circleimageview.CircleImageView;


public class FriendsReceivedAdapter extends RecyclerView.Adapter<FriendsReceivedAdapter.ViewHolder> {


    private static final int ROW_FRIENDS = R.layout.row_request_friend;

    private Context context;
    private List<User> myFriendsList;
    private List<User> receiverFriendsList;
    private List<User> friendsList;
    private boolean hasProgress = true;
    DatabaseReference databaseReference;

    private User currentUser;

    public FriendsReceivedAdapter(List<User> friendsList, User currentUser) {
        this.currentUser = currentUser;
        this.friendsList = friendsList;
        myFriendsList = new ArrayList<>();
        receiverFriendsList = new ArrayList<>();
        databaseReference = FirebaseDatabase.getInstance().getReference();

    }

    public FriendsReceivedAdapter() {
    }


    @Override
    public int getItemViewType(int position) {
        return ROW_FRIENDS;
    }


    @Override
    public void onAttachedToRecyclerView(RecyclerView recyclerView) {
        super.onAttachedToRecyclerView(recyclerView);
        context = recyclerView.getContext();
    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(viewType,
                parent, false);
        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolder holder, int position) {
        holder.tvName.setText(friendsList.get(position).getFullName());
        holder.btnAccept.setOnClickListener(v -> {
            acceptFriend(position);
        });

        holder.btnDecline.setOnClickListener(v -> {
            declineRequest(position);

        });

    }

    private void acceptFriend(int position) {
        UserRepo.getInstance().insertFriend(
                currentUser.getId(),
                friendsList.get(position),
                user -> {

                    UserRepo.getInstance().insertFriend(
                            friendsList.get(position).getId(),
                            currentUser,
                            user2 -> {
                                Toast.makeText(context, friendsList.get(position).getFullName()
                                        + " added to ur friend", Toast.LENGTH_SHORT).show();
                                declineRequest(position);

                            });
                });
    }

    private void declineRequest(int position) {
        FriendRequestRepo.getInstance().delete(friendsList.get(position).getRequestFriendId(),
                id -> {
                    //Deleted
                });
    }


    @Override
    public int getItemCount() {
        return friendsList.size();
    }


    public class ViewHolder extends RecyclerView.ViewHolder {
        @BindView(R.id.iv_user)
        CircleImageView ivUser;
        @BindView(R.id.tv_name)
        AppCompatTextView tvName;
        @BindView(R.id.btn_accept)
        AppCompatButton btnAccept;
        @BindView(R.id.btn_decline)
        AppCompatButton btnDecline;

        public ViewHolder(View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);

        }
    }
}
