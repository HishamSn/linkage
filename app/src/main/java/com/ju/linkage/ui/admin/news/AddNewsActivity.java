package com.ju.linkage.ui.admin.news;

import android.app.ProgressDialog;
import android.content.Intent;
import android.graphics.Bitmap;
import android.net.Uri;
import android.os.Bundle;
import android.provider.MediaStore;
import android.support.annotation.Nullable;
import android.support.v7.widget.AppCompatButton;
import android.support.v7.widget.AppCompatEditText;
import android.support.v7.widget.AppCompatImageView;
import android.view.View;
import android.widget.Toast;

import com.bumptech.glide.Glide;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.storage.FirebaseStorage;
import com.google.firebase.storage.StorageReference;
import com.google.firebase.storage.UploadTask;
import com.ju.linkage.R;
import com.ju.linkage.model.News;
import com.ju.linkage.repoistory.NewsRepo;
import com.ju.linkage.ui.base.BaseActivity;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

import static com.ju.linkage.constant.FirebaseConstant.TABLE_NEWS;

public class AddNewsActivity extends BaseActivity {

    private static final int PICK_IMAGE_REQUEST = 71;
    @BindView(R.id.et_title)
    AppCompatEditText etTitle;
    private Uri filePath;
    private FirebaseStorage storage;
    private String newsImageUrl;
    private DatabaseReference databaseNews;
    private List<News> newsList;
    StorageReference mStorageReference;

    @BindView(R.id.iv_news)
    AppCompatImageView ivNews;
    @BindView(R.id.et_details)
    AppCompatEditText etDetails;
    @BindView(R.id.btn_add)
    AppCompatButton btnSave;


    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_add_news);
        ButterKnife.bind(this);
        init();
    }

    private void init() {
        newsList = new ArrayList<>();
        mStorageReference = FirebaseStorage.getInstance().getReference();
    }


    @OnClick({R.id.iv_news, R.id.btn_add})
    public void onViewClicked(View view) {
        switch (view.getId()) {
            case R.id.iv_news:
                chooseImage();

                break;
            case R.id.btn_add:

                if (filePath != null) {
                    uploadNewsToFirebase();
                } else {
                    Toast.makeText(this,"Please Insert The photo",Toast.LENGTH_SHORT).show();
                }


                break;
        }
    }

    private void chooseImage() {
        Intent intent = new Intent();
        intent.setType("image/*");
        intent.setAction(Intent.ACTION_GET_CONTENT);
        startActivityForResult(Intent.createChooser(intent, "Select Picture"), PICK_IMAGE_REQUEST);
    }


    private void uploadNewsToFirebase() {
        databaseNews = FirebaseDatabase.getInstance().getReference().child(TABLE_NEWS);


        NewsRepo.getInstance().findAllSingle(newsList -> {
            this.newsList.clear();
            this.newsList.addAll(newsList);
            addToDatabase();
        });

    }

    private void upLoadPhotoToFirebase(int id) {
        if (filePath != null) {
            final ProgressDialog progressDialog = new ProgressDialog(this);
            progressDialog.setTitle(getString(R.string.Uploading));
            progressDialog.show();

            StorageReference storageRef = mStorageReference.child("news/" + id);

            UploadTask uploadTask = storageRef.putFile(filePath);
            uploadTask.continueWithTask(task -> {
                if (!task.isSuccessful()) {
                    progressDialog.dismiss();
                    Toast.makeText(AddNewsActivity.this, "Failed " + task.getException().getMessage(), Toast.LENGTH_SHORT).show();
                    throw task.getException();
                }
                return storageRef.getDownloadUrl();
            }).addOnCompleteListener(task -> {
                if (task.isSuccessful()) {
                    Uri downloadUri = task.getResult();
                    newsImageUrl = String.valueOf(downloadUri);

                    newsList.get(id).setImage(newsImageUrl);

                    databaseNews.child(String.valueOf(id)).setValue(newsList.get(id));
//                    databaseNews.child(id).child("image").setValue(newsImageUrl);
//                    databaseNews.child(id).child("active").setValue(true);

                    progressDialog.dismiss();
                    Toast.makeText(AddNewsActivity.this, "Uploaded", Toast.LENGTH_SHORT).show();
                    finish();

                } else {
                    progressDialog.dismiss();
                    Toast.makeText(AddNewsActivity.this, "Failed " + task.getException().getMessage(), Toast.LENGTH_SHORT).show();
                }
            });
        }
    }

    private void addToDatabase() {
        News news = new News();
        news.setId(newsList.size() + "");
        news.setDesc(etDetails.getText().toString());
        news.setTitle(etTitle.getText().toString());

        newsList.add(news);
        databaseNews.setValue(newsList);

        upLoadPhotoToFirebase(newsList.size() - 1);

    }


    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        if (requestCode == PICK_IMAGE_REQUEST && resultCode == RESULT_OK &&
                data != null && data.getData() != null) {
            filePath = data.getData();
            try {

                Bitmap bitmap = MediaStore.Images.Media.getBitmap(getContentResolver(), filePath);
                Glide.with(AddNewsActivity.this)
                        .load(bitmap)
                        .into(ivNews);


            } catch (Exception e) {
                Toast.makeText(AddNewsActivity.this, "Failed onActivityResult" + e.getMessage(), Toast.LENGTH_SHORT).show();

            }
        }
    }
}
