package com.ju.linkage.ui.student.chat;

import android.support.annotation.LayoutRes;
import android.support.annotation.NonNull;
import android.support.v7.widget.AppCompatTextView;
import android.support.v7.widget.RecyclerView;
import android.text.format.DateFormat;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.ju.linkage.R;
import com.ju.linkage.model.Chat;
import com.ju.linkage.prefs.PrefsUtils;

import java.util.Calendar;
import java.util.List;
import java.util.Locale;

import butterknife.BindView;
import butterknife.ButterKnife;

public class GroupChatAdapter extends RecyclerView.Adapter<GroupChatAdapter.ViewHolder> {


    @LayoutRes
    private int ROW_MY_MESSAGE = R.layout.row_chat_user;
    @LayoutRes
    private int ROW_PARTNER_MESSAGE = R.layout.row_chat_friend_group;
    private List<Chat> chatList;

    public GroupChatAdapter(List<Chat> chatList) {
        this.chatList = chatList;
    }

    @Override
    public int getItemViewType(int position) {


        if (chatList.get(position).getUser().getId().equals(PrefsUtils.getInstance().getId())) {
            return ROW_MY_MESSAGE;
        } else {
            return ROW_PARTNER_MESSAGE;
        }
    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {

        View view = LayoutInflater.from(viewGroup.getContext()).inflate(i, viewGroup, false);
        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolder viewHolder, int i) {
        if (!chatList.get(i).getUser().getId().equals(PrefsUtils.getInstance().getId())) {
            viewHolder.tvUserName.setText(chatList.get(i).getUser().getFullName());

        }


        viewHolder.tvMessage.setText(chatList.get(i).getText());
        Calendar cal = Calendar.getInstance(Locale.ENGLISH);
        cal.setTimeInMillis((Long.parseLong(chatList.get(i).getDate())) * 1000L);
        String date = DateFormat.format("hh:mm", cal).toString();

        viewHolder.tvTime.setText(date);

    }

    @Override
    public int getItemCount() {
        return chatList.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder {
        @BindView(R.id.tv_message)
        AppCompatTextView tvMessage;
        @BindView(R.id.tv_time)
        AppCompatTextView tvTime;
        AppCompatTextView tvUserName;

        public ViewHolder(@NonNull View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
            tvUserName = itemView.findViewById(R.id.tv_user_name);
        }
    }
}
