package com.ju.linkage.ui.student.report;

import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v7.widget.AppCompatEditText;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Spinner;
import android.widget.Toast;

import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;
import com.ju.linkage.R;
import com.ju.linkage.model.Report;
import com.ju.linkage.model.User;
import com.ju.linkage.repoistory.UserRepo;
import com.ju.linkage.ui.base.BaseActivity;
import com.ju.linkage.ui.student.main.MainActivity;
import com.ju.linkage.utils.Utils;
import com.yarolegovich.lovelydialog.LovelyProgressDialog;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

import static com.ju.linkage.constant.FirebaseConstant.TABLE_REPORT;

public class AddReportActivity extends BaseActivity {

    @BindView(R.id.sp_reasons)
    Spinner spReasons;
    @BindView(R.id.et_submit_msg)
    EditText etIssueDescription;
    @BindView(R.id.btn_submit)
    Button btnSubmit;
    @BindView(R.id.et_title)
    AppCompatEditText etTitle;

    private LovelyProgressDialog lovelyProgressDialog;

    private User user;
    private FirebaseAuth mAuth;
    //String issueDescription;
    private DatabaseReference databaseReferenceReport;
    private List<Report> reportList;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_add_report);
        ButterKnife.bind(this);

        init();

        getUserInfo();
        setupSpinnerAdapter();


    }

    private void getUserInfo() {
        UserRepo.getInstance().findById(mAuth.getUid(), student -> {
            user = student;
        });
    }

    private void init() {
        lovelyProgressDialog = Utils.getInstance().getProgressDialog(this);

        reportList = new ArrayList<>();
        mAuth = FirebaseAuth.getInstance();
        databaseReferenceReport = FirebaseDatabase.getInstance().getReference().child(TABLE_REPORT);


    }

    @OnClick({R.id.btn_submit})
    public void onViewClicked(View view) {
        //issueDescription = etIssueDescription.getText().toString();

        if (etIssueDescription.getText().toString().isEmpty()) {
            Toast.makeText(this, "Please write your description of your issue.",
                    Toast.LENGTH_SHORT).show();
        } else {
            saveIssueReportToDB();
        }

    }

    private void saveIssueReportToDB() {

lovelyProgressDialog.show();
        databaseReferenceReport.child(String.valueOf(spReasons.getSelectedItemPosition()))
                .addListenerForSingleValueEvent(new ValueEventListener() {
                    @Override
                    public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                        lovelyProgressDialog.dismiss();
                        reportList.clear();
                        Iterable<DataSnapshot> children = dataSnapshot.getChildren();
                        for (DataSnapshot child : children) {
                            Report report = child.getValue(Report.class);
                            reportList.add(report);
                        }

                        Report report = new Report();

                        report.setId(String.valueOf(spReasons.getSelectedItemPosition()));
                        report.setIssue(etTitle.getText().toString());
                        report.setIssueDescription(etIssueDescription.getText().toString());
                        report.setUser(user);

                        reportList.add(report);
                        databaseReferenceReport.child(String.valueOf(
                                spReasons.getSelectedItemPosition())).setValue(reportList);
                        etIssueDescription.getText().clear();

                    }

                    @Override
                    public void onCancelled(@NonNull DatabaseError databaseError) {
                        Toast.makeText(AddReportActivity.this,"issue hapend",Toast.LENGTH_SHORT).show();
                        lovelyProgressDialog.dismiss();

                    }
                });

    }


    private void setupSpinnerAdapter() {
        ArrayAdapter<CharSequence> spAdapter = ArrayAdapter.createFromResource(
                this, R.array.sp_reasons_array_list, R.layout.item_spinner_header);
        spAdapter.setDropDownViewResource(R.layout.row_spinner);
        spReasons.setAdapter(spAdapter);
    }


}
