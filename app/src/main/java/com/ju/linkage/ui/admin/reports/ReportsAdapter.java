package com.ju.linkage.ui.admin.reports;

import android.content.Context;
import android.content.Intent;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.ju.linkage.R;
import com.ju.linkage.model.Report;

import java.util.List;

public class ReportsAdapter extends RecyclerView.Adapter<ReportsAdapter.ViewHolder> {

    private List<Report> reportList;
    private Context context;


    public ReportsAdapter(List<Report> reportList) {
        this.reportList = reportList;
    }

    @Override
    public void onAttachedToRecyclerView(@NonNull RecyclerView recyclerView) {
        super.onAttachedToRecyclerView(recyclerView);
        context = recyclerView.getContext();
    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {

        View view = LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.row_reports, viewGroup, false);

        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolder viewHolder, int i) {

        viewHolder.tvIssueDescription.setText(reportList.get(i).getIssueDescription());
        viewHolder.itemView.setOnClickListener(v -> {
            Intent intent = new Intent(context, ReportDetailsActivity.class);
            intent.putExtra("name", reportList.get(i).getUser().getFullName());
            intent.putExtra("title", reportList.get(i).getIssue());
            intent.putExtra("description", reportList.get(i).getIssueDescription());
            context.startActivity(intent);
        });


    }

    @Override
    public int getItemCount() {
        return reportList.size();
    }

    class ViewHolder extends RecyclerView.ViewHolder {
        TextView tvIssueDescription;


        public ViewHolder(@NonNull View itemView) {
            super(itemView);
            tvIssueDescription = itemView.findViewById(R.id.tv_issue_description);
        }
    }
}
