package com.ju.linkage.ui.student.missing;

import android.content.Context;
import android.support.annotation.LayoutRes;
import android.support.annotation.NonNull;
import android.support.v7.app.AlertDialog;
import android.support.v7.widget.AppCompatImageButton;
import android.support.v7.widget.AppCompatImageView;
import android.support.v7.widget.AppCompatTextView;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;
import android.widget.Toast;

import com.bumptech.glide.Glide;
import com.ju.linkage.R;
import com.ju.linkage.model.Missing;
import com.ju.linkage.repoistory.MissingRepo;

import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;

public class MissingAdapter extends RecyclerView.Adapter<MissingAdapter.ViewHolder> {

    @LayoutRes
    private static final int ROW_MISSING = R.layout.row_missing;


    private List<Missing> missingList;
    private Context context;

    private String myId;


    public MissingAdapter(List<Missing> missingList, String myId) {
        this.missingList = missingList;
        this.myId = myId;
    }

    @Override
    public int getItemViewType(int position) {

        return ROW_MISSING;
    }

    @Override
    public void onAttachedToRecyclerView(@NonNull RecyclerView recyclerView) {
        super.onAttachedToRecyclerView(recyclerView);
        context = recyclerView.getContext();
    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int viewType) {
        View view = LayoutInflater.from(context).inflate(viewType, viewGroup, false);

        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolder viewHolder, int position) {
        viewHolder.tvMissing.setText(missingList.get(position).getText());
        viewHolder.tvPhone.setText(String.valueOf(missingList.get(position).getPhone()));
        Glide.with(context)
                .load(missingList.get(position).getImage())
                .into(viewHolder.ivImage);


        if (missingList.get(position).getUser().getId().equals(myId)) {
            viewHolder.ivDeleted.setVisibility(View.VISIBLE);
            viewHolder.ivDeleted.setOnClickListener(v -> removeMissing(position));

        }


    }


    private void removeMissing(int position) {

        AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(context);
        alertDialogBuilder.setMessage("Are you sure,You wanted to remove this missing item");

        alertDialogBuilder.setPositiveButton("yes", (arg0, arg1) -> {

                    MissingRepo.getInstance().delete(position, t -> {
                        Toast.makeText(context, "removed", Toast.LENGTH_SHORT).show();
                    });
                }

        );

        alertDialogBuilder.setNegativeButton("No", (dialog, which) -> {
            Toast.makeText(context, "Cancel", Toast.LENGTH_SHORT).show();

        });

        AlertDialog alertDialog = alertDialogBuilder.create();
        alertDialog.show();


    }

    @Override
    public int getItemCount() {
        return missingList.size();
    }

    class ViewHolder extends RecyclerView.ViewHolder {
        @BindView(R.id.iv_image)
        AppCompatImageView ivImage;
        @BindView(R.id.iv_delete)
        AppCompatImageButton ivDeleted;
        @BindView(R.id.tv_missing)
        AppCompatTextView tvMissing;
        @BindView(R.id.textView)
        TextView textView;
        @BindView(R.id.tv_phone)
        AppCompatTextView tvPhone;

        public ViewHolder(@NonNull View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
        }
    }
}
