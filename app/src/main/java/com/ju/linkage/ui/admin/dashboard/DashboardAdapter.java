package com.ju.linkage.ui.admin.dashboard;

import android.content.Context;
import android.content.Intent;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.ju.linkage.R;
import com.ju.linkage.model.ModelRowImage;
import com.ju.linkage.ui.admin.ads.AdsActivity;
import com.ju.linkage.ui.admin.news.NewsActivity;
import com.ju.linkage.ui.admin.reports.ReportsActivity;
import com.ju.linkage.ui.splash.SplashActivity;

import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;

public class DashboardAdapter extends RecyclerView.Adapter<DashboardAdapter.ViewHolder> {

    private List<ModelRowImage> modelRowImages;
    private Context context;

    public DashboardAdapter(List<ModelRowImage> modelRowImages) {
        this.modelRowImages = modelRowImages;
    }

    @Override
    public void onAttachedToRecyclerView(@NonNull RecyclerView recyclerView) {
        super.onAttachedToRecyclerView(recyclerView);
        this.context = recyclerView.getContext();
    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int viewType) {

        View view = LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.row_image, viewGroup, false);
        view.setLayoutParams(new RecyclerView.LayoutParams(
                ViewGroup.LayoutParams.MATCH_PARENT, viewGroup.getMeasuredHeight() / 3));

        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolder viewHolder, int position) {

        viewHolder.tvTitle.setText(modelRowImages.get(position).getTitle());
        viewHolder.ivImage.setBackground(context.getResources().getDrawable(modelRowImages.get(position).getImage()));
        viewHolder.itemView.setOnClickListener(v -> {
            switch (position) {
                case 0:
                    context.startActivity(new Intent(context, NewsActivity.class));

                    break;
                case 1:
                    context.startActivity(new Intent(context, AdsActivity.class));

                    break;
                case 2:
                    context.startActivity(new Intent(context, ReportsActivity.class));

                    break;
            }
        });


    }

    @Override
    public int getItemCount() {
        return modelRowImages.size();
    }

    class ViewHolder extends RecyclerView.ViewHolder {
        @BindView(R.id.tv_title)
        TextView tvTitle;

        @BindView(R.id.iv_main)
        ImageView ivImage;

        public ViewHolder(@NonNull View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);

        }
    }
}
