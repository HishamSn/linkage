package com.ju.linkage.ui.student.friends;

import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.app.AlertDialog;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.Toast;

import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;
import com.ju.linkage.R;
import com.ju.linkage.model.FriendRequest;
import com.ju.linkage.model.User;
import com.ju.linkage.repoistory.UserRepo;
import com.ju.linkage.utils.Utils;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

import static com.ju.linkage.constant.FirebaseConstant.TABLE_FRIENDS;
import static com.ju.linkage.constant.FirebaseConstant.TABLE_FRIEND_REQUEST;
import static com.ju.linkage.constant.FirebaseConstant.TABLE_USERS;

public class FriendsFragment extends Fragment {
    public static final String ARG_PAGE = "ARG_PAGE";


    @BindView(R.id.rv_friends)
    RecyclerView rvFriends;

    FirebaseUser firebaseUser;
    DatabaseReference databaseReferenceUser;
    DatabaseReference databaseReferenceFriendRequest;
    private int mPage;

    List<User> friendRequestList;
    List<User> myFriends;
    private FriendsReceivedDialog friendsReceivedDialog;

    public static FriendsFragment newInstance(int page) {
        Bundle args = new Bundle();
        args.putInt(ARG_PAGE, page);
        FriendsFragment fragment = new FriendsFragment();
        fragment.setArguments(args);
        return fragment;
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {

        View view = inflater.inflate(R.layout.fragment_friends, container, false);
        ButterKnife.bind(this, view);
        init();

        rvFriends.setLayoutManager(new LinearLayoutManager(getContext()));


        rvFriendsSetUp();

        UserRepo.getInstance().findById(firebaseUser.getUid(), user -> {
            user.setFriends(null);
            rvFriendsRequestSetUp(user);

        });

        return view;
    }

    private void rvFriendsSetUp() {
        databaseReferenceUser.child(firebaseUser.getUid()).child(TABLE_FRIENDS)
                .addValueEventListener(new ValueEventListener() {
                    @Override
                    public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                        Iterable<DataSnapshot> children = dataSnapshot.getChildren();
                        myFriends.clear();
                        for (DataSnapshot child : children) {
                            User myFriend = child.getValue(User.class);
                            myFriends.add(myFriend);
                        }
                        myFriends.add(0, new User());
                        rvFriends.setAdapter(new FriendsAdapter(myFriends));
                    }

                    @Override
                    public void onCancelled(@NonNull DatabaseError databaseError) {

                    }
                });
    }

    private void rvFriendsRequestSetUp(User user) {
        databaseReferenceFriendRequest.addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                Iterable<DataSnapshot> children = dataSnapshot.getChildren();
                friendRequestList.clear();
                for (DataSnapshot child : children) {
                    FriendRequest friendRequest = child.getValue(FriendRequest.class);
                    if (friendRequest.getReceiverId().equals(firebaseUser.getUid())) {

                        databaseReferenceUser.addListenerForSingleValueEvent(new ValueEventListener() {
                            @Override
                            public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                                Iterable<DataSnapshot> children = dataSnapshot.getChildren();
                                for (DataSnapshot child : children) {
                                    User user = child.getValue(User.class);
                                    if (friendRequest.getSenderId().equals(user.getId())) {
                                        user.setRequestFriendId(
                                                Utils.getInstance().sortString(friendRequest.getSenderId() + firebaseUser.getUid())
                                        );
                                        friendRequestList.add(user);
//                                        return;
                                    }
                                }

                                if (friendsReceivedDialog == null) {
                                    friendsReceivedDialog =
                                            new FriendsReceivedDialog(getActivity());
                                }

                                friendsReceivedDialog.rvFriendsReceived
                                        .setAdapter(new FriendsReceivedAdapter(friendRequestList, user));
                                if (!friendsReceivedDialog.isShowing()) {
                                    friendsReceivedDialog.show();
                                }
                            }

                            @Override
                            public void onCancelled(@NonNull DatabaseError databaseError) {

                            }
                        });
                    }
                }
                if (friendsReceivedDialog != null) {
                    if (friendRequestList.size() == 0) {
//                        friendsReceivedDialog.rvFriendsReceived.getAdapter().notifyDataSetChanged();

                        if (friendsReceivedDialog.isShowing()) {
                            friendsReceivedDialog.cancel();
                        }


                    }

                }
            }

            @Override
            public void onCancelled(@NonNull DatabaseError databaseError) {

            }
        });
    }

    private void init() {
        friendRequestList = new ArrayList<>();
        myFriends = new ArrayList<>();
        firebaseUser = FirebaseAuth.getInstance().getCurrentUser();
        databaseReferenceUser = FirebaseDatabase.getInstance().getReference().child(TABLE_USERS);
        databaseReferenceFriendRequest = FirebaseDatabase.getInstance().getReference().child(TABLE_FRIEND_REQUEST);
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
    }

    @OnClick(R.id.fab)
    public void onViewClicked() {

        AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(getContext());
        alertDialogBuilder.setMessage("Add A Friend");

        final EditText etId = new EditText(getContext());

        alertDialogBuilder.setView(etId);

        alertDialogBuilder.setPositiveButton("Add", (arg0, arg1) -> {

                    if (etId.getText().toString().isEmpty()) {
                        Toast.makeText(getContext(), "Please Fill The id", Toast.LENGTH_SHORT).show();
                        return;
                    }

                    databaseReferenceUser.addListenerForSingleValueEvent(new ValueEventListener() {
                        @Override
                        public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                            Iterable<DataSnapshot> children = dataSnapshot.getChildren();
                            boolean isFind = false;

                            for (DataSnapshot child : children) {
                                User friendUser = child.getValue(User.class);

                                if (isIdEqualUniversityId(etId, friendUser)) {
                                    isFind = true;
                                    if (isIdFriendEqualId(friendUser)) {
                                        Toast.makeText(getContext(), "You can't add yourself", Toast.LENGTH_SHORT).show();
                                        return;
                                    }

                                    if (isAlreadyFriend(friendUser)) {
                                        Toast.makeText(getContext(), "this friend you have already", Toast.LENGTH_SHORT).show();
                                        return;
                                    }

                                    createFriendRequest(friendUser);
                                    return;
                                }
                            }
                            if (!isFind) {
                                Toast.makeText(getContext(), "this user not found", Toast.LENGTH_SHORT).show();
                            }
                        }

                        @Override
                        public void onCancelled(@NonNull DatabaseError databaseError) {

                        }
                    });
                }
        );

        alertDialogBuilder.setNegativeButton("Cancel", (dialog, which) -> {
        });

        AlertDialog alertDialog = alertDialogBuilder.create();
        alertDialog.show();


        firebaseUser.getUid();

    }

    private boolean isAlreadyFriend(User friendUser) {
        if (friendUser.getFriends() == null) {
            return false;
        }


        for (int i = 0; i < friendUser.getFriends().size(); i++) {
            if (friendUser.getFriends().get(i).getId().equals(firebaseUser.getUid())) {
                return true;
            }

        }


        return false;
    }

    private boolean isIdEqualUniversityId(EditText etId, User friendUser) {
        if (etId.getText().toString().equals(friendUser.getUniversityID())) {
            return true;
        }
        return false;
    }

    private boolean isIdFriendEqualId(User friendUser) {
        return firebaseUser.getUid().equals(friendUser.getId());
    }


//    private void acceptFriend(int position) {
//        UserRepo.getInstance().insertFriend(
//                currentUser.getId(),
//                friendsList.get(position),
//                user -> {
//
//                    UserRepo.getInstance().insertFriend(
//                            friendsList.get(position).getId(),
//                            currentUser,
//                            user2 -> {
//                                Toast.makeText(context, friendsList.get(position).getFullName()
//                                        + " added to ur friend", Toast.LENGTH_SHORT).show();
//                                declineRequest(position);
//
//                            });
//                });
//    }


    private void createFriendRequest(User friendUser) {


        databaseReferenceFriendRequest.addListenerForSingleValueEvent(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                Iterable<DataSnapshot> children = dataSnapshot.getChildren();
                boolean isFind = false;
                for (DataSnapshot child : children) {
                    FriendRequest friendRequest = child.getValue(FriendRequest.class);
                    if (friendRequest.getSenderId().equals(firebaseUser.getUid()) &&
                            friendRequest.getReceiverId().equals(friendUser.getId())
                    ) {
                        isFind = true;
                        Toast.makeText(getContext(),
                                "Request sent Already to " + friendUser.getFullName(),
                                Toast.LENGTH_SHORT).show();
                    } else if (friendRequest.getSenderId().equals(friendUser.getId()) &&
                            friendRequest.getReceiverId().equals(firebaseUser.getUid())) {


                    }
                }

                if (!isFind) {
                    String sortId = Utils.getInstance().sortString(friendUser.getId() + firebaseUser.getUid());

                    FriendRequest friendRequest = new FriendRequest(friendUser.getId(), firebaseUser.getUid());
                    databaseReferenceFriendRequest.child(sortId).setValue(friendRequest);
                    Toast.makeText(getContext(),
                            friendUser.getFullName() + " Added", Toast.LENGTH_SHORT).show();
                }
            }

            @Override
            public void onCancelled(@NonNull DatabaseError databaseError) {

            }
        });
    }
}
