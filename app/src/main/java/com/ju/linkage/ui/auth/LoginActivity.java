package com.ju.linkage.ui.auth;

import android.app.ActivityOptions;
import android.content.Context;
import android.content.Intent;
import android.os.Build;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.design.widget.FloatingActionButton;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.CardView;
import android.view.View;
import android.widget.EditText;
import android.widget.Toast;

import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;
import com.ju.linkage.R;
import com.ju.linkage.constant.AppConstant;
import com.ju.linkage.model.User;
import com.ju.linkage.prefs.PrefsUtils;
import com.ju.linkage.ui.admin.dashboard.DashboardActivity;
import com.ju.linkage.ui.base.BaseActivity;
import com.ju.linkage.ui.student.main.MainActivity;
import com.ju.linkage.utils.LocalHelper;
import com.mobsandgeeks.saripaar.ValidationError;
import com.mobsandgeeks.saripaar.Validator;
import com.mobsandgeeks.saripaar.annotation.Length;
import com.yarolegovich.lovelydialog.LovelyProgressDialog;

import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

import static com.ju.linkage.constant.AppConstant.EN;
import static com.ju.linkage.constant.FirebaseConstant.ADMIN_ACCOUNT;
import static com.ju.linkage.constant.FirebaseConstant.TABLE_USERS;
import static com.mobsandgeeks.saripaar.Validator.ValidationListener;

public class LoginActivity extends BaseActivity implements ValidationListener {

    @BindView(R.id.cv)
    CardView cv;
    @BindView(R.id.fab)
    FloatingActionButton fab;

    @Length(min = 7, max = 7, messageResId = R.string.wrong_university_id)
    @BindView(R.id.et_university_id)
    EditText etId;

    @Length(min = 5, messageResId = R.string.wrong_password)
    @BindView(R.id.et_password)
    EditText etPassword;
    private FirebaseAuth mAuth;
    private LovelyProgressDialog waitingDialog;


    Validator validator;
    private DatabaseReference mDatabaseReference;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);
        ButterKnife.bind(this);
        init();
    }




    private void init() {
        mAuth = FirebaseAuth.getInstance();
        mDatabaseReference = FirebaseDatabase.getInstance().getReference().child("Users");
        waitingDialog = new LovelyProgressDialog(this).setCancelable(false);
        validator = new Validator(this);
        validator.setValidationListener(this);
    }

    @OnClick({R.id.btn_login, R.id.fab})
    public void onViewClicked(View view) {
        switch (view.getId()) {
            case R.id.btn_login:

                validator.validate();


                break;
            case R.id.fab:
                getWindow().setExitTransition(null);
                getWindow().setEnterTransition(null);

                if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
                    ActivityOptions options =
                            ActivityOptions.makeSceneTransitionAnimation(this, fab, fab.getTransitionName());
                    startActivityForResult(new Intent(this, RegisterActivity.class), AppConstant.REQUEST_CODE_REGISTER, options.toBundle());
                } else {
                    startActivityForResult(new Intent(this, RegisterActivity.class), AppConstant.REQUEST_CODE_REGISTER);
                }
                break;
        }
    }

    private void login(User user, String password) {
        if (etId.getText().toString().isEmpty() || etPassword.getText().toString().isEmpty()) {
            Toast.makeText(this, "please fill data", Toast.LENGTH_SHORT).show();
            return;
        }
        waitingDialog.setIcon(R.drawable.ic_person_low)
                .setTitle("Login....")
                .setTopColorRes(R.color.colorPrimary)
                .show();

        mAuth.signInWithEmailAndPassword(user.getEmail(), password)
                .addOnCompleteListener(this, task -> {
                    waitingDialog.dismiss();
                    if (task.isSuccessful()) {

                        if (user.getEmail().equalsIgnoreCase(ADMIN_ACCOUNT)) {
                            PrefsUtils.getInstance().setAdmin(true);
                            goToActivity(DashboardActivity.class);
                        } else {
                            PrefsUtils.getInstance().setId(user.getId());
                            PrefsUtils.getInstance().setStudentName(user.getFullName());
                            goToActivity(MainActivity.class);
                        }
                        finish();

                    } else {
                        Toast.makeText(this, task.getException().getMessage(), Toast.LENGTH_SHORT).show();
                    }
                });
    }

    private void goToActivity(Class activity) {
        startActivity(new Intent(this, activity));
    }


    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == AppConstant.REQUEST_CODE_REGISTER && resultCode == RESULT_OK) {
            createUser(data.getStringExtra("id"),
                    data.getStringExtra("fullName")
                    , data.getStringExtra("phone")
                    , data.getStringExtra("email")
                    , data.getStringExtra("password")
            );
        }
    }


    public void createUser(String universityId, String fullName, String phone, String email,
                           String password) {
        waitingDialog.setIcon(R.drawable.ic_add_friend)
                .setTitle("Registering....")
                .setTopColorRes(R.color.colorPrimary)
                .show();


        mAuth.createUserWithEmailAndPassword(email, password)
                .addOnCompleteListener(this, task -> {
                    waitingDialog.dismiss();
                    if (task.isSuccessful()) {

                        Toast.makeText(this, "success", Toast.LENGTH_SHORT).show();
                        User user = new User();
                        user.setId(mAuth.getCurrentUser().getUid());
                        user.setUniversityID(universityId);
                        user.setEmail(email);
                        user.setFullName(fullName);
                        user.setPhone(phone);
                        mDatabaseReference.child(mAuth.getCurrentUser().getUid()).setValue(user);
//                        startActivity(new Intent(this, MainActivity.class));
//                        finish();

                    } else {
                        Toast.makeText(this, task.getException().getMessage(), Toast.LENGTH_SHORT).show();

                    }
                });
    }


    private void checkUniversityId() {

        mDatabaseReference = FirebaseDatabase.getInstance().getReference().child(TABLE_USERS);
        mDatabaseReference.addListenerForSingleValueEvent(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                Iterable<DataSnapshot> children = dataSnapshot.getChildren();
                for (DataSnapshot child : children) {
                    User user = child.getValue(User.class);
                    if (user.getUniversityID().equals(etId.getText().toString())) {
                        login(user, etPassword.getText().toString());
                        return;
                    }
                }
                Toast.makeText(getApplicationContext(), "this student does not exist ", Toast.LENGTH_SHORT).show();

            }

            @Override
            public void onCancelled(@NonNull DatabaseError databaseError) {

            }
        });
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        setResult(RESULT_CANCELED, null);
        finish();
    }

    @Override
    public void onValidationSucceeded() {
        checkUniversityId();
    }

    @Override
    public void onValidationFailed(List<ValidationError> errors) {
        for (ValidationError error : errors) {
            View view = error.getView();
            String message = error.getCollatedErrorMessage(this);
            Toast.makeText(this, message, Toast.LENGTH_SHORT).show();
        }
    }
}
