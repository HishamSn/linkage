package com.ju.linkage.ui.admin.ads;

import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.design.widget.FloatingActionButton;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.RecyclerView;

import com.ju.linkage.R;
import com.ju.linkage.repoistory.AdsRepo;
import com.ju.linkage.ui.base.BaseActivity;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

public class AdsActivity extends BaseActivity {
    @BindView(R.id.rv_ads)
    RecyclerView rvAds;
    @BindView(R.id.fab)
    FloatingActionButton fab;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.fragment_ads);
        ButterKnife.bind(this);


        AdsRepo.getInstance().findAllRunTime(adsList ->
        {
            rvAds.setAdapter(new AdsAdapter(adsList));
        });


    }


    @OnClick(R.id.fab)
    public void onViewClicked() {
        startActivity(new Intent(AdsActivity.this, AddAdsActivity.class));
    }
}