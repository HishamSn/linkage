package com.ju.linkage.ui.student.links;

import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.ju.linkage.R;
import com.ju.linkage.model.Links;

import java.util.List;

public class LinksAdapter extends RecyclerView.Adapter<LinksAdapter.ViewHolder> {

    private List<Links> linksList;
    private static final int ROW_LINKS_URL = R.layout.row_name;
    private static final int ROW_LINKS_TITLE = R.layout.row_name_title;
    private Context context;

    public LinksAdapter(List<Links> linksList) {
        this.linksList = linksList;
    }

    @Override
    public void onAttachedToRecyclerView(@NonNull RecyclerView recyclerView) {
        super.onAttachedToRecyclerView(recyclerView);
        context = recyclerView.getContext();
    }

    @Override
    public int getItemViewType(int position) {
        if (position == 0 || position == 11
                || position == 16 || position == 22
                || position == 28 || position == 31) {
            return ROW_LINKS_TITLE;
        }

        return ROW_LINKS_URL;
    }

    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int viewType) {
        View view = LayoutInflater.from(viewGroup.getContext()).inflate(viewType, viewGroup, false);

        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolder viewHolder, int position) {
        viewHolder.tvTitle.setText(linksList.get(position).getTitle());

        if (linksList.get(position).getUrl() != null) {
            viewHolder.itemView.setOnClickListener(v -> {
                Uri uri = Uri.parse(linksList.get(position).getUrl());
                Intent intent = new Intent(Intent.ACTION_VIEW, uri);
                context.startActivity(intent);
            });
        }
    }


    @Override
    public int getItemCount() {
        return linksList.size();
    }

    class ViewHolder extends RecyclerView.ViewHolder {
        TextView tvTitle;

        public ViewHolder(@NonNull View itemView) {
            super(itemView);
            tvTitle = itemView.findViewById(R.id.tv_name);
        }
    }
}
