package com.ju.linkage.ui.student.ads;


import android.content.Context;
import android.content.Intent;
import android.support.annotation.NonNull;
import android.support.v7.app.AlertDialog;
import android.support.v7.widget.AppCompatImageView;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Toast;

import com.bumptech.glide.Glide;
import com.ju.linkage.R;
import com.ju.linkage.model.Ads;
import com.ju.linkage.repoistory.AdsRepo;
import com.ju.linkage.ui.admin.ads.AdsActivity;

import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;

public class AdsAdapter extends RecyclerView.Adapter<AdsAdapter.ViewHolder> {
    private static final int ROW_ADS = R.layout.row_ads;
    private Context context;
    private List<Ads> adsList;

    public AdsAdapter(List<Ads> adsList) {
        this.adsList = adsList;
    }

    @Override
    public int getItemViewType(int position) {

        return ROW_ADS;
    }

    @Override
    public void onAttachedToRecyclerView(@NonNull RecyclerView recyclerView) {
        super.onAttachedToRecyclerView(recyclerView);
        context = recyclerView.getContext();
    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int viewType) {
        View view = LayoutInflater.from(viewGroup.getContext()).inflate(viewType, viewGroup, false);


        return new com.ju.linkage.ui.student.ads.AdsAdapter.ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolder holder, int position) {

        Glide.with(context)
                .load(adsList.get(position).getImage())
                .into(holder.ivImage);

        holder.itemView.setOnClickListener(v -> {
//            Intent intent = new Intent(context, AdsActivity.class);
//            intent.putExtra("image", adsList.get(position).getImage());
//            context.startActivity(intent);

        });


        holder.itemView.setOnLongClickListener(v -> {
            //  removeAds(position);
            return false;
        });

    }

    private void removeAds(int position) {

        AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(context);
        alertDialogBuilder.setMessage("Are you sure,You wanted to remove this ads");

        alertDialogBuilder.setPositiveButton("Yes", (arg0, arg1) -> {

                    AdsRepo.getInstance().delete(position, t -> {
                        Toast.makeText(context, "removed", Toast.LENGTH_SHORT).show();
                    });
                }

        );

        alertDialogBuilder.setNegativeButton("No", (dialog, which) -> {
            Toast.makeText(context, "Cancel", Toast.LENGTH_SHORT).show();

        });

        AlertDialog alertDialog = alertDialogBuilder.create();
        alertDialog.show();


    }

    @Override
    public int getItemCount() {
        return adsList.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder {
        @BindView(R.id.iv_image)
        AppCompatImageView ivImage;

        public ViewHolder(@NonNull View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);

        }
    }
}

