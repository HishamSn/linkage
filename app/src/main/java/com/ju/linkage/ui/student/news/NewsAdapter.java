package com.ju.linkage.ui.student.news;

import android.content.Context;
import android.content.Intent;
import android.support.annotation.LayoutRes;
import android.support.annotation.NonNull;
import android.support.v7.widget.AppCompatImageView;
import android.support.v7.widget.AppCompatTextView;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.bumptech.glide.Glide;
import com.ju.linkage.R;
import com.ju.linkage.model.News;

import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;

public class NewsAdapter extends RecyclerView.Adapter<NewsAdapter.ViewHolder> {

    @LayoutRes
    private static final int ROW_NEWS = R.layout.row_news;

    private List<News> newsList;
    private Context context;


    public NewsAdapter(List<News> newsList) {
        this.newsList = newsList;
    }

    @Override
    public int getItemViewType(int position) {

        return ROW_NEWS;
    }

    @Override
    public void onAttachedToRecyclerView(@NonNull RecyclerView recyclerView) {
        super.onAttachedToRecyclerView(recyclerView);
        context = recyclerView.getContext();
    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int viewType) {
        View view = LayoutInflater.from(context).inflate(viewType, viewGroup, false);

        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolder viewHolder, int position) {

        viewHolder.tvNews.setText(newsList.get(position).getTitle());
        Glide.with(context)
                .load(newsList.get(position).getImage())
                .into(viewHolder.ivImage);

        viewHolder.itemView.setOnClickListener(v -> {
            Intent intent = new Intent(context, NewsDetailsActivity.class);
            intent.putExtra("image", newsList.get(position).getImage());
            intent.putExtra("text", newsList.get(position).getText());
            intent.putExtra("title", newsList.get(position).getTitle());
            intent.putExtra("desc", newsList.get(position).getDesc());
            context.startActivity(intent);

        });

    }

    @Override
    public int getItemCount() {
        return newsList.size();
    }

    class ViewHolder extends RecyclerView.ViewHolder {
        @BindView(R.id.iv_image)
        AppCompatImageView ivImage;
        @BindView(R.id.tv_news)
        AppCompatTextView tvNews;

        public ViewHolder(@NonNull View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
        }
    }
}
