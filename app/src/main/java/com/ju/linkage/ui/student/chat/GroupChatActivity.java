package com.ju.linkage.ui.student.chat;

import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.design.widget.FloatingActionButton;
import android.support.v7.widget.AppCompatEditText;
import android.support.v7.widget.AppCompatTextView;
import android.support.v7.widget.RecyclerView;
import android.view.KeyEvent;
import android.view.inputmethod.EditorInfo;

import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;
import com.ju.linkage.R;
import com.ju.linkage.model.Chat;
import com.ju.linkage.model.User;
import com.ju.linkage.prefs.PrefsUtils;
import com.ju.linkage.repoistory.UserRepo;
import com.ju.linkage.ui.base.BaseActivity;

import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.TimeUnit;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import de.hdodenhof.circleimageview.CircleImageView;

import static com.ju.linkage.constant.FirebaseConstant.TABLE_ROOMS;

public class GroupChatActivity extends BaseActivity {

    @BindView(R.id.iv_user)
    CircleImageView ivUser;
    @BindView(R.id.tv_name)
    AppCompatTextView tvName;
    @BindView(R.id.rv_chat)
    RecyclerView rvChat;
    @BindView(R.id.et_chat_content)
    AppCompatEditText etChatContent;
    DatabaseReference databaseReference;
    @BindView(R.id.fab_send)
    FloatingActionButton fabSend;

    private List<Chat> chatList;
    private String myId;
    private User user;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_chat);
        ButterKnife.bind(this);

        tvName.setText(getIntent().getExtras().getString("name"));
        myId = PrefsUtils.getInstance().getId();

        init();
//        initListenerActions();


        UserRepo.getInstance().findById(myId, user -> {
            this.user = user;
            user.setFriends(null);
        });

        databaseReference.child(TABLE_ROOMS).child("Group")
                .addValueEventListener(new ValueEventListener() {
                    @Override
                    public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                        chatList.clear();
                        Iterable<DataSnapshot> children = dataSnapshot.getChildren();
                        for (DataSnapshot child : children) {
                            Chat chat = child.getValue(Chat.class);
                            chatList.add(chat);
                        }
                        rvChat.setAdapter(new GroupChatAdapter(chatList));
                    }

                    @Override
                    public void onCancelled(@NonNull DatabaseError databaseError) {
                    }
                });
//        rvChat.smoothScrollToPosition(rvChat.getAdapter().getItemCount() - 1);
    }

    private void init() {
        chatList = new ArrayList<>();
        databaseReference = FirebaseDatabase.getInstance().getReference();

    }

    private void initListenerActions() {

        etChatContent.setOnKeyListener((v, keyCode, event) -> {
            if (event.getAction() == KeyEvent.ACTION_DOWN) {
                switch (keyCode) {
                    case KeyEvent.KEYCODE_DPAD_CENTER:
                    case KeyEvent.KEYCODE_ENTER:
                        fabSend.performClick();
                        return true;
                    default:
                        break;
                }
            }
            return false;
        });


        etChatContent.setOnEditorActionListener((v, actionId, event) -> {
            boolean handled = false;
            if (actionId == EditorInfo.IME_ACTION_SEND) {
                fabSend.performClick();
                handled = true;
            }
            return handled;
        });
    }

    @OnClick(R.id.fab_send)
    public void onViewClicked() {
        if (!etChatContent.getText().toString().trim().isEmpty()) {

            Chat chat = new Chat();
            chat.setId(chatList.size());
            chat.setText(etChatContent.getText().toString());
            chat.setDate(String.valueOf(TimeUnit.MILLISECONDS.toSeconds(System.currentTimeMillis())));
            chat.setUser(user);

            chatList.add(chat);
            databaseReference.child(TABLE_ROOMS).child("Group")
                    .setValue(chatList).addOnCompleteListener(task -> etChatContent.setText(""));

        }
    }
}
