package com.ju.linkage.ui.splash;

import android.app.Notification;
import android.app.PendingIntent;
import android.content.Intent;
import android.os.Build;
import android.os.Bundle;
import android.support.v4.app.NotificationCompat;
import android.support.v4.app.NotificationManagerCompat;
import android.support.v7.widget.AppCompatTextView;
import android.widget.ImageView;
import android.widget.RemoteViews;

import com.daimajia.androidanimations.library.Techniques;
import com.daimajia.androidanimations.library.YoYo;
import com.github.florent37.viewanimator.AnimationListener;
import com.github.florent37.viewanimator.ViewAnimator;
import com.ju.linkage.BuildConfig;
import com.ju.linkage.R;
import com.ju.linkage.fcm.NotificationReceiver;
import com.ju.linkage.prefs.PrefsUtils;
import com.ju.linkage.repoistory.SettingRepo;
import com.ju.linkage.ui.admin.dashboard.DashboardActivity;
import com.ju.linkage.ui.auth.LoginActivity;
import com.ju.linkage.ui.base.BaseActivity;
import com.ju.linkage.ui.student.main.MainActivity;
import com.ju.linkage.utils.SessionUtils;
import com.squareup.picasso.Picasso;

import butterknife.BindView;
import butterknife.ButterKnife;
import cn.pedant.SweetAlert.SweetAlertDialog;

public class SplashActivity extends BaseActivity {

    @BindView(R.id.iv_logo)
    ImageView ivLogo;
    @BindView(R.id.iv_name)
    ImageView ivName;
    @BindView(R.id.tv_version)
    AppCompatTextView tvVersion;

    private AnimationListener.Stop stopAnimation;
    private NotificationManagerCompat notificationManager;
    private boolean isVersionUpdated;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_splash);
        ButterKnife.bind(this);
        notificationManager = NotificationManagerCompat.from(this);

        tvVersion.setText("V" + BuildConfig.VERSION_NAME);


        SettingRepo.getInstance().getVersionEventListener(version -> {
            if (version != BuildConfig.VERSION_CODE) {
                isVersionUpdated = true;
                showNotification(version);
            }


        });


        Picasso.with(this).load(R.drawable.logo_empty).into(ivLogo);
        Picasso.with(this).load(R.drawable.linkage_text).into(ivName);
        openActivityAnimationStop();

        initAnimation();


        YoYo.with(Techniques.BounceIn)
                .duration(1500)
                .playOn(ivName);

    }

    private void openActivityAnimationStop() {
        stopAnimation = () -> {
            if (isVersionUpdated) {

                new SweetAlertDialog(this)
                        .setTitleText("Update Application")
                        .setContentText("Please Update Version")
                        .setConfirmClickListener(sDialog -> {
                            new SweetAlertDialog(SplashActivity.this)
                                    .setTitleText("Bye Bye")
                                    .setConfirmClickListener(dialog -> {
                                        sDialog.dismissWithAnimation();
                                       finish();
                                    })
                                    .show();

                            sDialog.dismissWithAnimation();
                        })
                        .show();

            } else {
                if (SessionUtils.getInstance().isLogin()) {
                    if (PrefsUtils.getInstance().isAdmin()) {
                        goToActivity(DashboardActivity.class);
                    } else {
                        goToActivity(MainActivity.class);
                    }
                } else {
                    goToActivity(LoginActivity.class);
                }

                finish();
            }


        };
    }

    private void goToActivity(Class activity) {
        startActivity(new Intent(this, activity));
    }


    private void initAnimation() {
        ViewAnimator
                .animate(ivLogo)
                .rotation(1000)
                .scale(0, 1)
                .rotationX(180)
                .onStop(stopAnimation)
                .start();
    }


    public void showNotification(int version) {
        RemoteViews collapsedView = new RemoteViews(getPackageName(),
                R.layout.notification_collapsed);
        RemoteViews expandedView = new RemoteViews(getPackageName(),
                R.layout.noteification_expaneded);

        Intent clickIntent = new Intent(this, NotificationReceiver.class);
        PendingIntent clickPendingIntent = PendingIntent.getBroadcast(this,
                0, clickIntent, 0);

        collapsedView.setTextViewText(R.id.tv_collased_1, "Update Application");
        collapsedView.setTextViewText(R.id.tv_collased_2, "you must update the app to Version " + version);

        expandedView.setImageViewResource(R.id.iv_expanded, R.drawable.logo_empty);
        expandedView.setOnClickPendingIntent(R.id.iv_expanded, clickPendingIntent);

        Notification notification = new NotificationCompat.Builder(this, "1")
                .setSmallIcon(R.drawable.ic_about)
                .setCustomContentView(collapsedView)
                .setCustomBigContentView(expandedView)
                //.setStyle(new NotificationCompat.DecoratedCustomViewStyle())
                .build();

        notificationManager.notify(1, notification);
    }
}
