package com.ju.linkage.ui.student.missing;

import android.content.Intent;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.design.widget.FloatingActionButton;
import android.support.v7.app.ActionBar;
import android.support.v7.widget.RecyclerView;

import com.google.firebase.auth.FirebaseAuth;
import com.ju.linkage.R;
import com.ju.linkage.repoistory.MissingRepo;
import com.ju.linkage.ui.base.BaseActivity;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

public class MissingActivity extends BaseActivity {
    @BindView(R.id.rv_missing)
    RecyclerView rvMissing;
    @BindView(R.id.fab)
    FloatingActionButton fab;
    private FirebaseAuth mAuth;


    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_missing_items);
        ButterKnife.bind(this);
        setActionBar("Missing Student Items");
        init();

        MissingRepo.getInstance().findAllRunTime(missingList ->
        {
            rvMissing.setAdapter(new MissingAdapter(missingList, mAuth.getUid()));
        });


    }

    private void init() {
        mAuth = FirebaseAuth.getInstance();
    }

    public void setActionBar(String heading) {
        // TODO Auto-generated method stub

        ActionBar actionBar = getSupportActionBar();
        actionBar.setHomeButtonEnabled(true);
        actionBar.setDisplayHomeAsUpEnabled(true);
        actionBar.setDisplayShowHomeEnabled(true);
        actionBar.setBackgroundDrawable(new ColorDrawable(getResources().getColor(R.color.colorPrimary)));
        actionBar.setTitle(heading);
        actionBar.show();

    }

    @Override
    public boolean onSupportNavigateUp() {
        onBackPressed();
        return true;
    }

    @OnClick(R.id.fab)
    public void onViewClicked() {
        startActivity(new Intent(MissingActivity.this, AddMissingActivity.class));
    }
}
