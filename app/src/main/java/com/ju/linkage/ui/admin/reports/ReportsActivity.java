package com.ju.linkage.ui.admin.reports;

import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Spinner;

import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;
import com.ju.linkage.R;
import com.ju.linkage.model.Report;
import com.ju.linkage.ui.base.BaseActivity;
import com.ju.linkage.utils.Utils;
import com.yarolegovich.lovelydialog.LovelyProgressDialog;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;

import static com.ju.linkage.constant.FirebaseConstant.TABLE_REPORT;

public class ReportsActivity extends BaseActivity implements AdapterView.OnItemSelectedListener {

    @BindView(R.id.rv_all_reports)
    RecyclerView rvIssueDescription;

    @BindView(R.id.sp_reasons)
    Spinner spReasons;

    private List<Report> reportList;
    private DatabaseReference databaseReferenceReport;
    private LovelyProgressDialog lovelyProgressDialog;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_reports);
        ButterKnife.bind(this);

        init();
        setupSpinnerAdapter();

    }

    private void init() {
        lovelyProgressDialog = Utils.getInstance().getProgressDialog(this);

        reportList = new ArrayList<>();
        databaseReferenceReport = FirebaseDatabase.getInstance().getReference().child(TABLE_REPORT);
        spReasons.setOnItemSelectedListener(this);
    }

    private void setupSpinnerAdapter() {
        ArrayAdapter<CharSequence> spAdapter = ArrayAdapter.createFromResource(
                this, R.array.sp_reasons_array_list, R.layout.item_spinner_header);
        spAdapter.setDropDownViewResource(R.layout.row_spinner);
        spReasons.setAdapter(spAdapter);
    }


    @Override
    public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {

        lovelyProgressDialog.show();
        databaseReferenceReport.child(position + "").addListenerForSingleValueEvent(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                lovelyProgressDialog.dismiss();
                reportList.clear();
                Iterable<DataSnapshot> children = dataSnapshot.getChildren();
                for (DataSnapshot child : children) {
                    Report report = child.getValue(Report.class);

                    reportList.add(report);
                }
                rvIssueDescription.setAdapter(new ReportsAdapter(reportList));
            }

            @Override
            public void onCancelled(@NonNull DatabaseError databaseError) {
                lovelyProgressDialog.dismiss();

            }
        });
    }

    @Override
    public void onNothingSelected(AdapterView<?> parent) {

    }
}
