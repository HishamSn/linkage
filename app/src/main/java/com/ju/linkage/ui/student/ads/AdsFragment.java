package com.ju.linkage.ui.student.ads;

import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.design.widget.FloatingActionButton;
import android.support.v4.app.Fragment;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.ju.linkage.R;
import com.ju.linkage.model.Ads;
import com.ju.linkage.model.News;
import com.ju.linkage.repoistory.AdsRepo;
import com.ju.linkage.repoistory.NewsRepo;
import com.ju.linkage.ui.admin.news.NewsAdapter;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.Unbinder;

public class AdsFragment extends Fragment {

    @BindView(R.id.rv_ads)
    RecyclerView rvAds;
    Unbinder unbinder;
    @BindView(R.id.fab)
    FloatingActionButton fab;

    private List<Ads> adsList;


    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_ads, container, false);
//        Q 2 unbinder
        unbinder = ButterKnife.bind(this, view);
        fab.hide();


        AdsRepo.getInstance().findAllRunTime(adsList ->
        {
            rvAds.setAdapter(new AdsAdapter(adsList));
        });


        init();

        return view;
    }

    private void init() {
        adsList = new ArrayList<>();
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
//        rvAds.setAdapter(new AdsAdapter(adsList));

    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
    }
}
