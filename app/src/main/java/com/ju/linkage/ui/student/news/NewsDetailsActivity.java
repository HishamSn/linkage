package com.ju.linkage.ui.student.news;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.widget.AppCompatTextView;
import android.support.v7.widget.Toolbar;
import android.widget.ImageView;

import com.bumptech.glide.Glide;
import com.ju.linkage.R;
import com.ju.linkage.ui.base.BaseActivity;

import butterknife.BindView;
import butterknife.ButterKnife;

public class NewsDetailsActivity extends BaseActivity {
    @BindView(R.id.iv_image)
    ImageView ivImage;
    @BindView(R.id.tv_details)
    AppCompatTextView tvDetails;
    @BindView(R.id.toolbar)
    Toolbar toolbar;


    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_news_details);
        ButterKnife.bind(this);

        toolbar.setTitle(getIntent().getExtras().getString("title"));


        tvDetails.setText(getIntent().getExtras().getString("desc"));
        Glide.with(this)
                .load(getIntent().getExtras().getString("image"))
                .into(ivImage);
    }
}
