package com.ju.linkage.ui.admin.news;

import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.design.widget.FloatingActionButton;
import android.support.v7.widget.RecyclerView;

import com.ju.linkage.R;
import com.ju.linkage.repoistory.NewsRepo;
import com.ju.linkage.ui.base.BaseActivity;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

public class NewsActivity extends BaseActivity {

    @BindView(R.id.rv_news)
    RecyclerView rvNews;
    @BindView(R.id.fab)
    FloatingActionButton fab;

    //    private List<News> newsList;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.fragment_news);
        ButterKnife.bind(this);
//        init();

        NewsRepo.getInstance().findAllRunTime(newsList ->
        {
            rvNews.setAdapter(new NewsAdapter(newsList));
        });


    }

//    private void init() {
//        newsList = new ArrayList<>();
//    }

    @OnClick(R.id.fab)
    public void onViewClicked() {
        startActivity(new Intent(NewsActivity.this, AddNewsActivity.class));
    }
}
