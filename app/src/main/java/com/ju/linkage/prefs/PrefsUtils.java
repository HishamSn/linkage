package com.ju.linkage.prefs;

import android.content.SharedPreferences;
import android.preference.PreferenceManager;

import com.ju.linkage.common.MyApplication;


public class PrefsUtils {

    private static final String ADMIN = "admin";
    private static final String STUDENT_FULL_NAME = "full_name";
    private static final String ID = "id";

    private static PrefsUtils instance;
    private SharedPreferences prefs;

    private PrefsUtils() {
        prefs = PreferenceManager.getDefaultSharedPreferences(MyApplication.getInstance());
    }

    public static synchronized PrefsUtils getInstance() {
        if (instance == null) {
            instance = new PrefsUtils();
        }
        return instance;
    }


    public boolean isAdmin() {
        return prefs.getBoolean(ADMIN, false);
    }

    public void setAdmin(boolean enable) {
        prefs.edit().putBoolean(ADMIN, enable).apply();
    }


    public String getStudentName() {
        return prefs.getString(STUDENT_FULL_NAME, "unknown");
    }

    public void setStudentName(String name) {
        prefs.edit().putString(STUDENT_FULL_NAME, name).apply();
    }

    public String getId() {
        return prefs.getString(ID, "unknown");
    }

    public void setId(String id) {
        prefs.edit().putString(ID, id).apply();
    }

}
