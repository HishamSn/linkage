package com.ju.linkage.utils;


import android.content.Context;

import com.ju.linkage.R;
import com.yarolegovich.lovelydialog.LovelyProgressDialog;

import java.util.Arrays;

public class Utils {
    private static Utils instance;


    private Utils() {

    }

    public static synchronized Utils getInstance() {

        if (instance == null) {
            instance = new Utils();
        }
        return instance;
    }

    public String sortString(String inputString) {
        // convert input string to char array
        char tempArray[] = inputString.toCharArray();

        // sort tempArray
        Arrays.sort(tempArray);

        // return new sorted string
        return new String(tempArray);
    }


    public LovelyProgressDialog getProgressDialog(Context context) {
        LovelyProgressDialog waitingDialog = new LovelyProgressDialog(context).setCancelable(false);
        waitingDialog.setCancelable(true)
                .setTitle("Loading....")
                .setTopColorRes(R.color.colorPrimary);

        return waitingDialog;
    }


}
