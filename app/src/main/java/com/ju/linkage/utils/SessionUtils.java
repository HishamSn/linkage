package com.ju.linkage.utils;

import android.annotation.SuppressLint;
import android.support.annotation.NonNull;

import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;
import com.ju.linkage.model.User;
import com.ju.linkage.prefs.PrefsUtils;

import static com.ju.linkage.constant.FirebaseConstant.TABLE_USERS;


public class SessionUtils {

    private static SessionUtils instance;
    private User user;
    private FirebaseAuth mAuth;
    //    private MyCars myCar;
    private DatabaseReference databaseReference;

    @SuppressLint("CheckResult")
    private SessionUtils() {
        mAuth = FirebaseAuth.getInstance();
        getUserData();

    }

    public static synchronized SessionUtils getInstance() {

        if (instance == null) {
            instance = new SessionUtils();
        }
        return instance;
    }

    public FirebaseAuth mAuth() {
        return mAuth;
    }

    public User getUser() {
        if (user == null) {
            getUserData();
        }
        return user;
    }

    public void logout() {
        PrefsUtils.getInstance().setAdmin(false);
        mAuth.signOut();
        user = null;
    }

    public boolean isLogin() {
        return mAuth.getCurrentUser() != null;
    }


    public DatabaseReference getDatabaseReference() {
        return FirebaseDatabase.getInstance().getReference()
                .child(TABLE_USERS).child(mAuth().getUid());

    }


    private void getUserData() {
        if (mAuth().getUid() == null) {
            return;
        }
        databaseReference = FirebaseDatabase.getInstance().getReference()
                .child(TABLE_USERS).child(mAuth().getUid());
        databaseReference.addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                user = dataSnapshot.getValue(User.class);
            }

            @Override
            public void onCancelled(@NonNull DatabaseError databaseError) {

            }
        });
    }
}
