package com.ju.linkage.model;

public class Report {
    private String id ;
    private String issue;
    private String issueDescription;
    private User user;

    public Report() {
    }

    public Report(String id, String issue, String issueDescription, User user) {
        this.id = id;
        this.issue = issue;
        this.issueDescription = issueDescription;
        this.user = user;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getIssue() {
        return issue;
    }

    public void setIssue(String issue) {
        this.issue = issue;
    }

    public String getIssueDescription() {
        return issueDescription;
    }

    public void setIssueDescription(String issueDescription) {
        this.issueDescription = issueDescription;
    }

    public User getUser() {
        return user;
    }

    public void setUser(User user) {
        this.user = user;
    }
}
