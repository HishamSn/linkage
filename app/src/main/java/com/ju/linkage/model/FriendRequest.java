package com.ju.linkage.model;

public class FriendRequest {
    private String receiverId;
    private String senderId;

    public FriendRequest() {
    }

    public FriendRequest(String receiverId, String senderId) {
        this.receiverId = receiverId;
        this.senderId = senderId;
    }

    public String getReceiverId() {
        return receiverId;
    }

    public void setReceiverId(String receiverId) {
        this.receiverId = receiverId;
    }

    public String getSenderId() {
        return senderId;
    }

    public void setSenderId(String senderId) {
        this.senderId = senderId;
    }
}
