package com.ju.linkage.model;

import android.os.Parcel;
import android.os.Parcelable;

import java.util.List;

public class User implements Parcelable {

    private String id;
    private String fullName;
    private String email;
    private String phone;
    private String universityID;

    private List<UserFriend> Friends;

    private String requestFriendId;

    public static final Parcelable.Creator<User> CREATOR = new Parcelable.Creator<User>() {
        @Override
        public User createFromParcel(Parcel source) {
            return new User(source);
        }

        @Override
        public User[] newArray(int size) {
            return new User[size];
        }
    };


    public User() {
    }

    public User(Parcel in) {
        this.id = in.readString();
        this.fullName = in.readString();
        this.email = in.readString();
        this.phone = in.readString();
        this.universityID = in.readString();
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getFullName() {
        return fullName;
    }

    public void setFullName(String fullName) {
        this.fullName = fullName;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getUniversityID() {
        return universityID;
    }

    public void setUniversityID(String universityID) {
        this.universityID = universityID;
    }

    public String getPhone() {
        return phone;
    }

    public void setPhone(String phone) {
        this.phone = phone;
    }

    public String getRequestFriendId() {
        return requestFriendId;
    }

    public List<UserFriend> getFriends() {
        return Friends;
    }

    public void setFriends(List<UserFriend> friends) {
        Friends = friends;
    }

    public void setRequestFriendId(String requestFriendId) {
        this.requestFriendId = requestFriendId;
    }

    @Override
    public String toString() {
        return "User{" +
                "id='" + id + '\'' +
                ", fullName='" + fullName + '\'' +
                ", email='" + email + '\'' +
                ", phone='" + phone + '\'' +
                ", universityID='" + universityID + '\'' +
                ", Friends=" + Friends +
                ", requestFriendId='" + requestFriendId + '\'' +
                '}';
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(this.id);
        dest.writeString(this.fullName);
        dest.writeString(this.email);
        dest.writeString(this.phone);
        dest.writeString(this.universityID);
    }
}
